function resolved_max (s : type is array( (<>) ) of <>) return s'element is 
  variable result : s'element := s'left ; 
begin
  for i in s'range loop
    if (s(i) /= s'left) then 
      if result = s'left then
        result := s(i) ;
      else
        result := s'right ;
      end if ; 
    end if ; 
  end loop ;
  return result ; 
end function resolved_max ; 
