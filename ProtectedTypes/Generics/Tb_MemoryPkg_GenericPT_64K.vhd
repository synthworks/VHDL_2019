--
--  File Name:         Tb_MemoryPkg_GenericPT_64k.vhd
--  Design Unit Name:  Tb_MemoryPkg_GenericPT_64k
--  Revision:          STANDARD VERSION
--
--  Copyright (c) 2015-2021 by SynthWorks Design Inc.  All rights reserved.
--
--  SynthWorks Confidential.  No distribution without written authorization.
--  To only be used for testing the STANDARD version of RandomPkg.
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis      email:  jim@synthworks.com
--
--  Description:
--    Test Suite for RandomPkg.vhd
--
--  Developed for:
--		SynthWorks Design Inc.
--		Training Courses
--		11898 SW 128th Ave.
--		Tigard, Or  97223
--		http://www.SynthWorks.com
--
--
--  Revision History:
--    Date      Version    Description
--    04/2015   2015.04    Formalizing MemoryPkg tests
--    12/2020   2020.12    Updated test for Generics on PT
--    05/2021   2021.05    Updated for VHDL-2019 Just the New Stuff Part2
--
--

library osvvm ;
context osvvm.OsvvmContext ;

use work.MemoryPkg_2019.all ;  


library IEEE ;
use ieee.std_logic_1164.all ; 
use ieee.numeric_std.all ;
use ieee.numeric_std_unsigned.all ; 


entity Tb_MemoryPkg_GenericPT_64k is 
end Tb_MemoryPkg_GenericPT_64k ;

architecture MemoryPkg_GenericPT_64k of Tb_MemoryPkg_GenericPT_64k is 

  constant ADDR_LEN  : integer := 16 ; 
  constant DATA_LEN  : integer := ADDR_LEN ; 

  shared variable Mem : work.MemoryPkg_2019.MemoryPType generic map (
      AddrWidth   =>  ADDR_LEN, 
      DataWidth   =>  DATA_LEN
    ) ;  

  signal   SyncPoint, TestDone   : integer_barrier ;
  
  constant MAX_VALUE : integer := 2**ADDR_LEN-1 ; 
  constant ALL_U     : std_logic_vector(DATA_LEN-1 downto 0) := (others => 'U') ; 

begin

-- --------------------------------------------------
-- ControlProc 
--   Responsible for test initialization and finalization
-- --------------------------------------------------
  ControlProc : process
  begin
    SetAlertLogName("MemoryPkg_GenericPT_64k") ;
    TranscriptOpen("./results/MemoryPkg_GenericPT_64k.txt") ;
    SetTranscriptMirror(TRUE) ; 
    wait for 0 ns ; 
    
    WaitForBarrier(TestDone, 5 ms) ; 
    
    print("");
    ReportAlerts ; 
    TranscriptClose ; 
--    AlertIfDiff("./results/MemoryPkg_GenericPT_64k.txt", "../ProtectedTypes/Generics/validated_results/MemoryPkg_GenericPT_64k.txt", "results file mismatch") ; 
    if not IsTranscriptMirrored then 
      ReportAlerts ; 
    end if ;
    std.env.stop( GetAlertCount) ; 
    wait ;
  end process ControlProc ;


-- --------------------------------------------------
-- WriteProc 
--   Modeled in a simlar fashion to a testbench with an 
--   interface that drives stimulus.
--   It took extra work to make that happen here, so 
--   I would probably not use this form as a general 
--   case for testing memories.
-- --------------------------------------------------
  WriteProc : process
  begin
    -- Waiting for ReadProc to Read and Check values
    WaitForBarrier(SyncPoint) ;  -- Wait for ReadProc to Finish
    print(LF & "Writing 0 to " & to_string(MAX_VALUE)) ; 
    for i in 0 to MAX_VALUE loop 
      Mem.MemWrite(to_slv(i,ADDR_LEN), to_slv(i,DATA_LEN) ) ; 
      wait for 1 ns ; 
    end loop ; 
    
    WaitForBarrier(SyncPoint) ;  -- Start ReadProc
    -- Waiting for ReadProc to Read and Check values
    WaitForBarrier(SyncPoint) ;  -- Wait for ReadProc to Finish
    
    print(LF & "Writing " & to_string(MAX_VALUE) & " downto 0.  Value is MAX_VALUE - i") ; 
    for i in MAX_VALUE downto 0 loop 
      Mem.MemWrite(to_slv(i,ADDR_LEN), to_slv(MAX_VALUE - i,DATA_LEN) ) ; 
      wait for 1 ns ; 
    end loop ; 
    
    WaitForBarrier(SyncPoint) ;  -- Start ReadProc
    -- Waiting for ReadProc to Read and Check values
    WaitForBarrier(SyncPoint) ;  -- Wait for ReadProc to Finish
    
    print(LF & "Erasing Memory - Should read as U") ; 
    Mem.MemErase ;
    
    WaitForBarrier(SyncPoint) ;  -- Start ReadProc
    -- Waiting for ReadProc to Read and Check values
    WaitForBarrier(SyncPoint) ;  -- Wait for ReadProc to Finish
    
    print(LF & "Writing " & to_string((MAX_VALUE+1)/2) & " to " & to_string(MAX_VALUE)) ; 
    for i in (MAX_VALUE+1)/2 to MAX_VALUE loop 
      Mem.MemWrite(to_slv(i,ADDR_LEN), to_slv(i,DATA_LEN) ) ; 
      wait for 1 ns ; 
    end loop ; 
    print("Writing 0 to " & to_string(MAX_VALUE/2)) ; 
    for i in 0 to MAX_VALUE/2 loop 
      Mem.MemWrite(to_slv(i,ADDR_LEN), to_slv(i,DATA_LEN) ) ; 
      wait for 1 ns ; 
    end loop ; 
    
    WaitForBarrier(SyncPoint) ;  -- Start ReadProc
    -- Waiting for ReadProc to Read and Check values
    WaitForBarrier(TestDone) ;  -- Wait for ReadProc to Finish
    
    wait ; 
  end process WriteProc ; 


-- --------------------------------------------------
-- ReadProc 
--   Modeled in a simlar fashion to a testbench with an 
--   interface that receives transactions.
--   It took extra work to make that happen here, so 
--   I would probably not use this form as a general 
--   case for testing memories.
-- --------------------------------------------------
  ReadProc : process
    variable rData : std_logic_vector(DATA_LEN-1 downto 0) ; 
  begin
    print("Reading Uninitialized Memory - Expecting all U") ; 
    for i in 0 to MAX_VALUE loop 
      Mem.MemRead(to_slv(i,ADDR_LEN), rData) ;  -- Procedure
      AffirmIfEqual(rData, ALL_U) ; 
      wait for 1 ns ; 
    end loop ; 
    
    WaitForBarrier(SyncPoint) ;  -- Start WriteProc
    -- Waiting for WriteProc to Write to Memory
    WaitForBarrier(SyncPoint) ;  -- Wait for WriteProc to Finish
    
    print("Reading 0 to " & to_string(MAX_VALUE)) ; 
    for i in 0 to MAX_VALUE loop 
      Mem.MemRead(to_slv(i,ADDR_LEN), rData) ;  -- Procedure
      AffirmIfEqual(rData, to_slv(i,DATA_LEN)) ; 
      wait for 1 ns ; 
    end loop ; 
    
    WaitForBarrier(SyncPoint) ;  -- Start WriteProc
    -- Waiting for WriteProc to Write to Memory
    WaitForBarrier(SyncPoint) ;  -- Wait for WriteProc to Finish
    
    print("Reading " & to_string(MAX_VALUE) & " downto 0.  Value is MAX_VALUE - i") ; 
    for i in MAX_VALUE downto 0 loop 
      rData := Mem.MemRead(to_slv(i,ADDR_LEN)) ; 
      AffirmIfEqual(rData, to_slv(MAX_VALUE - i,DATA_LEN)) ; 
      wait for 1 ns ; 
    end loop ; 
    
    WaitForBarrier(SyncPoint) ;  -- Start WriteProc
    -- Waiting for WriteProc to Write to Memory
    WaitForBarrier(SyncPoint) ;  -- Wait for WriteProc to Finish
    
    print("Reading Erased Memory - Expecting all U") ; 
    for i in 0 to MAX_VALUE loop 
      rData := Mem.MemRead(to_slv(i,ADDR_LEN)) ; 
      AffirmIfEqual(rData, ALL_U) ; 
      wait for 1 ns ; 
    end loop ; 
    
    WaitForBarrier(SyncPoint) ;  -- Start WriteProc
    -- Waiting for WriteProc to Write to Memory
    WaitForBarrier(SyncPoint) ;  -- Wait for WriteProc to Finish
    
    print("Reading 0 to " & to_string(MAX_VALUE)) ; 
    for i in 0 to MAX_VALUE loop 
      rData := Mem.MemRead(to_slv(i,ADDR_LEN)) ; 
      AffirmIfEqual(rData, to_slv(i,DATA_LEN)) ; 
      wait for 1 ns ; 
    end loop ; 
    
    WaitForBarrier(TestDone) ;  -- TestDone

    wait ; 
  end process ReadProc ; 

end MemoryPkg_GenericPT_64k ; 