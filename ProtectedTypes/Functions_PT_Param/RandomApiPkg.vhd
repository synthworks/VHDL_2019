--
--  File Name:         RandomApiPkg.vhd
--  Design Unit Name:  RandomApiPkg
--  Revision:          STANDARD VERSION
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis          SynthWorks
--
--
--  Package Defines
--    Refits RandomPkg with a conventional call API
--    This package requires VHDL-2019
--
--  Developed for:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        11898 SW 128th Ave.  Tigard, Or  97223
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    05/2021:  2021.05    Initial revision
--
--
--  This file is part of OSVVM.
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  

library osvvm ; 
  context osvvm.OsvvmContext ; 
  
use std.textio.all ;

package RandomApiPkg is
--    -- Seed Manipulation
--    -- Known ambiguity between InitSeed with string and integer_vector
--    -- Recommendation, use :  RV.InitSeed(RV'instance_path) ;
--    -- For integer_vector use either : RV.InitSeed(IV => (1,5)) ;
--    --   or : RV.InitSeed(integer_vector'(1,5)) ;
  procedure InitSeed (RV : inout RandomPType ; S  : string ) ;
--    procedure InitSeed (RV : inout RandomPType ; I  : integer ) ;
--    procedure InitSeed (RV : inout RandomPType ; IV : integer_vector ) ;
--
--    -- SetSeed & GetSeed :  Used to save and restore seed values
--    procedure SetSeed (RandomSeedIn : RandomSeedType ) ;
--    impure function GetSeed return RandomSeedType ;
--    -- SeedRandom = SetSeed & GetSeed for SV compatibility
--    -- replace with aliases when they work in popular simulators
--    procedure SeedRandom (RandomSeedIn : RandomSeedType ) ;
--    impure function SeedRandom return RandomSeedType ;
--    -- alias SeedRandom is SetSeed [RandomSeedType] ;
--    -- alias SeedRandom is GetSeed [return RandomSeedType] ;
--
--    -- Setting Randomization Parameters
--    -- Allows RandInt to have distributions other than uniform
--    procedure SetRandomParm (RandomParmIn : RandomParmType) ;
--    procedure SetRandomParm (
--      Distribution : RandomDistType ;
--      Mean         : Real := 0.0 ;
--      Deviation    : Real := 0.0
--    ) ;
--    impure function GetRandomParm return RandomParmType ;
--    impure function GetRandomParm return RandomDistType ;
--
--    -- For compatibility with previous version - replace with alias
--    procedure SetRandomMode (RandomDistIn : RandomDistType) ;
--    -- alias SetRandomMode is SetRandomParm [RandomDistType, Real, Real] ;
--
--    --  Base Randomization Distributions
--    -- Uniform :   Generate a random number with a Uniform distribution
--    impure function Uniform (Min, Max : in real) return real ;
--    impure function Uniform (Min, Max : integer) return integer ;
--    impure function Uniform (Min, Max : integer ; Exclude : integer_vector) return integer ;
--
--    -- FavorSmall
--    --   Generate random numbers with a greater number of small
--    --   values than large values
--    impure function FavorSmall (Min, Max : real) return real ;
--    impure function FavorSmall (Min, Max : integer) return integer ;
--    impure function FavorSmall (Min, Max : integer ; Exclude : integer_vector) return integer ;
--
--    -- FavorBig
--    --   Generate random numbers with a greater number of large
--    --   values than small values
--    impure function FavorBig (Min, Max : real) return real ;
--    impure function FavorBig (Min, Max : integer) return integer ;
--    impure function FavorBig (Min, Max : integer ; Exclude : integer_vector) return integer ;
--
--    -- Normal :   Generate a random number with a normal distribution
--    impure function Normal (Mean, StdDeviation : real) return real ;
--    -- Normal + RandomVal >= Min and RandomVal < Max
--    impure function Normal (Mean, StdDeviation, Min, Max : real) return real ;
--    impure function Normal (
--      Mean          : real ;
--      StdDeviation  : real ;
--      Min           : integer ;
--      Max           : integer ;
--      Exclude       : integer_vector := NULL_INTV
--    ) return integer ;
--
--    -- Poisson :  Generate a random number with a poisson distribution
--    --   Discrete distribution = only generates integral values
--    impure function Poisson (Mean : real) return real ;
--    -- Poisson + RandomVal >= Min and RandomVal < Max
--    impure function Poisson (Mean, Min, Max : real) return real ;
--    impure function Poisson (
--      Mean          : real ;
--      Min           : integer ;
--      Max           : integer ;
--      Exclude       : integer_vector := NULL_INTV
--    ) return integer ;
--
--    -- randomization with a range
  impure function RandInt (RV : inout RandomPType ; Min, Max : integer) return integer ;
--    impure function RandReal(Min, Max : Real) return real ;
--    impure function RandTime (Min, Max : time ; Unit : time := ns) return time ;
--    impure function RandSlv (Min, Max, Size : natural) return std_logic_vector ;
--    impure function RandUnsigned (Min, Max, Size : natural) return Unsigned ;
--    impure function RandSigned (Min, Max : integer ; Size : natural ) return Signed ;
--    impure function RandIntV (Min, Max : integer ; Size : natural) return integer_vector ;
--    impure function RandIntV (Min, Max : integer ; Unique : natural ; Size : natural) return integer_vector ;
--    impure function RandRealV (Min, Max : real ; Size : natural) return real_vector ;
--    impure function RandTimeV (Min, Max : time ; Size : natural ; Unit : time := ns) return time_vector ;
--    impure function RandTimeV (Min, Max : time ; Unique : natural ; Size : natural ; Unit : time := ns) return time_vector ;
--
--    --  randomization with a range and exclude vector
--    impure function RandInt (Min, Max : integer ; Exclude : integer_vector ) return integer ;
--    impure function RandTime (Min, Max : time ; Exclude : time_vector ; Unit : time := ns) return time ;
--    impure function RandSlv (Min, Max : natural ; Exclude : integer_vector ; Size : natural ) return std_logic_vector ;
--    impure function RandUnsigned (Min, Max : natural ; Exclude : integer_vector ; Size : natural ) return Unsigned ;
--    impure function RandSigned (Min, Max : integer ; Exclude : integer_vector ; Size : natural ) return Signed ;
--    impure function RandIntV (Min, Max : integer ; Exclude : integer_vector ; Size : natural) return integer_vector ;
--    impure function RandIntV (Min, Max : integer ; Exclude : integer_vector ; Unique : natural ; Size : natural) return integer_vector ;
--    impure function RandTimeV (Min, Max : time ; Exclude : time_vector ; Size : natural ; Unit : in time := ns) return time_vector ;
--    impure function RandTimeV (Min, Max : time ; Exclude : time_vector ; Unique : natural ; Size : natural ; Unit : in time := ns) return time_vector ;
--
--    -- Randomly select a value within a set of values
--    impure function RandInt ( A : integer_vector ) return integer ;
--    impure function RandReal ( A : real_vector ) return real ;
--    impure function RandTime (A : time_vector) return time ;
--    impure function RandSlv (A : integer_vector ; Size : natural) return std_logic_vector  ;
--    impure function RandUnsigned (A : integer_vector ; Size : natural) return Unsigned ;
--    impure function RandSigned (A : integer_vector ; Size : natural ) return Signed ;
--    impure function RandIntV (A : integer_vector ; Size : natural) return integer_vector ;
--    impure function RandIntV (A : integer_vector ; Unique : natural ; Size : natural) return integer_vector ;
--    impure function RandRealV (A : real_vector ; Size : natural) return real_vector ;
--    impure function RandRealV (A : real_vector ; Unique : natural ; Size : natural) return real_vector ;
--    impure function RandTimeV (A : time_vector ; Size : natural) return time_vector ;
--    impure function RandTimeV (A : time_vector ; Unique : natural ; Size : natural) return time_vector ;
--
--    -- Randomly select a value within a set of values with exclude values (so can skip last or last n)
--    impure function RandInt ( A, Exclude : integer_vector  ) return integer ;
--    impure function RandReal ( A, Exclude : real_vector ) return real ;
--    impure function RandTime (A, Exclude : time_vector) return time ;
--    impure function RandSlv (A, Exclude : integer_vector ; Size : natural) return std_logic_vector  ;
--    impure function RandUnsigned (A, Exclude : integer_vector ; Size : natural) return Unsigned ;
--    impure function RandSigned (A, Exclude : integer_vector ; Size : natural ) return Signed ;
--    impure function RandIntV (A, Exclude : integer_vector ; Size : natural) return integer_vector ;
--    impure function RandIntV (A, Exclude : integer_vector ; Unique : natural ; Size : natural) return integer_vector ;
--    impure function RandRealV (A, Exclude : real_vector ; Size : natural) return real_vector ;
--    impure function RandRealV (A, Exclude : real_vector ; Unique : natural ; Size : natural) return real_vector ;
--    impure function RandTimeV (A, Exclude : time_vector ; Size : natural) return time_vector ;
--    impure function RandTimeV (A, Exclude : time_vector ; Unique : natural ; Size : natural) return time_vector ;
--
--    -- Randomly select between 0 and N-1 based on the specified weight.
--    -- where N = number values in weight array
--    impure function DistInt ( Weight : integer_vector ) return integer ;
--    impure function DistSlv ( Weight : integer_vector ; Size  : natural ) return std_logic_vector ;
--    impure function DistUnsigned ( Weight : integer_vector ; Size  : natural ) return unsigned ;
--    impure function DistSigned ( Weight : integer_vector ; Size  : natural ) return signed ;
--    impure function DistBool ( Weight : NaturalVBoolType ) return boolean ;
--    impure function DistSl ( Weight : NaturalVSlType ) return std_logic ;
--    impure function DistBit ( Weight : NaturalVBitType ) return bit ;
--
--    -- Distribution with just weights and with exclude values
--    impure function DistInt ( Weight : integer_vector ; Exclude : integer_vector ) return integer ;
--    impure function DistSlv ( Weight : integer_vector ; Exclude : integer_vector ; Size  : natural ) return std_logic_vector ;
--    impure function DistUnsigned ( Weight : integer_vector ; Exclude : integer_vector ; Size  : natural ) return unsigned ;
--    impure function DistSigned ( Weight : integer_vector ; Exclude : integer_vector ; Size  : natural ) return signed ;
--
--    -- Distribution with weight and value
--    impure function DistValInt ( A : DistType ) return integer ;
--    impure function DistValSlv ( A : DistType ; Size  : natural) return std_logic_vector ;
--    impure function DistValUnsigned ( A : DistType ; Size  : natural) return unsigned ;
--    impure function DistValSigned ( A : DistType ; Size  : natural) return signed ;
--
--    -- Distribution with weight and value and with exclude values
--    impure function DistValInt ( A : DistType ; Exclude : integer_vector ) return integer ;
--    impure function DistValSlv ( A : DistType ; Exclude : integer_vector ; Size  : natural) return std_logic_vector ;
--    impure function DistValUnsigned ( A : DistType ; Exclude : integer_vector ; Size  : natural) return unsigned ;
--    impure function DistValSigned ( A : DistType ; Exclude : integer_vector ; Size  : natural) return signed ;
--
--    -- Large vector handling.
--    impure function RandUnsigned (Size : natural) return unsigned ;
--    impure function RandSlv (Size : natural) return std_logic_vector ;
--    impure function RandSigned (Size : natural) return signed ;
--    impure function RandUnsigned (Max : Unsigned) return unsigned ;
--    impure function RandSlv (Max : std_logic_vector) return std_logic_vector ;
--    impure function RandSigned (Max : signed) return signed ;
--    impure function RandUnsigned (Min, Max : unsigned) return unsigned ;
--    impure function RandSlv (Min, Max : std_logic_vector) return std_logic_vector ;
--    impure function RandSigned (Min, Max : signed) return signed ;
--
--    -- Convenience Functions
--    impure function RandReal return real ; -- 0.0 to 1.0
--    impure function RandReal(Max : Real) return real ; -- 0.0 to Max
--    impure function RandInt (Max : integer) return integer ;
--    impure function RandSlv (Max, Size : natural) return std_logic_vector ;
--    impure function RandUnsigned (Max, Size : natural) return Unsigned ;
--    impure function RandSigned (Max : integer ; Size : natural ) return Signed ;
--    impure function RandBool return boolean;
--    impure function RandSl return std_logic;


end package RandomApiPkg ;

--- ///////////////////////////////////////////////////////////////////////////
--- ///////////////////////////////////////////////////////////////////////////
--- ///////////////////////////////////////////////////////////////////////////

package body RandomApiPkg is
  procedure InitSeed (RV : inout RandomPType ; S  : string ) is
  begin
    RV.InitSeed(S) ; 
  end procedure InitSeed ; 

  impure function RandInt (
    RV        : inout RandomPType ;
    Min, Max  :       integer
  ) return integer is
  begin
    return RV.RandInt(Min, Max) ;
  end function RandInt ;


end package body RandomApiPkg ;