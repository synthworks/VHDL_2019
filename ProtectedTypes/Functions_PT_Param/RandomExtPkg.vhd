--
--  File Name:         RandomExtPkg.vhd
--  Design Unit Name:  RandomExtPkg
--  Revision:          STANDARD VERSION
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis          SynthWorks
--
--
--  Package Defines
--    Shows how to extend functionality in RandomPkg 
--    Using a more normal function call notation.
--    This package requires VHDL-2019
--
--  Developed for:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        11898 SW 128th Ave.  Tigard, Or  97223
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    05/2021:  2021.05    Initial revision
--
--
--  This file is part of OSVVM.
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  

library osvvm ; 
  context osvvm.OsvvmContext ; 
  
use std.textio.all ;

package RandomExtPkg is
  impure function NormalRealV (
    variable RV   : inout RandomPType ;
    Mean          : in    real ;
    StdDeviation  : in    real ;
    Size          : in    integer
  ) return real_vector ;
end package RandomExtPkg ;

--- ///////////////////////////////////////////////////////////////////////////
--- ///////////////////////////////////////////////////////////////////////////
--- ///////////////////////////////////////////////////////////////////////////

package body RandomExtPkg is
  impure function NormalRealV (
    variable RV   : inout RandomPType ;
    Mean          : in    real ;
    StdDeviation  : in    real ;
    Size          : in    integer
  ) return real_vector is
    variable result : real_vector(Size-1 downto 0); 
  begin
    for I in result'range loop
      result(i) := RV.Normal(Mean, StdDeviation) ; 
    end loop ; 
    return result ;
  end function NormalRealV ;

end package body RandomExtPkg ;