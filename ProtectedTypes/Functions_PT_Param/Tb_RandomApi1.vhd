--
--  File Name:         Tb_RandomApi1.vhd
--  Design Unit Name:  Tb_RandomApi1
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis      jim@synthworks.com
--
--
--  Description:
--      Basic Test for RandInt with conventional call syntax
--
--
--  Developed by:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    05/2021   2021.05    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  
library ieee ;
  use ieee.std_logic_1164.all ;
  use ieee.numeric_std.all ;
  use ieee.numeric_std_unsigned.all ;
  use ieee.math_real.all ;
  
  use std.env.all ;
  use std.textio.all ;

library osvvm ;
  context osvvm.OsvvmContext ;
    
  use work.RandomApiPkg.all ; 
        
entity Tb_RandomApi1 is
end entity Tb_RandomApi1 ; 
architecture Test of Tb_RandomApi1 is
    
begin

  testProc : process
    variable RV : RandomPType ;
    variable IntV : integer_vector(0 to 9) := (others => 0) ; 
    variable Int  : integer ; 
  begin
    InitSeed(RV, RV'instance_name) ;
    
    for i in 1 to IntV'length * 1000 loop 
      Int       := RandInt(RV, 0, 9) ; 
      IntV(Int) := IntV(Int) + 1 ; 
    end loop ;
    
    for i in IntV'range loop
      print(to_string(i) & " occurred " & to_string(IntV(i)) & " times.") ; 
    end loop ;

    wait ; 
  end process testProc ; 
  

end architecture Test ;