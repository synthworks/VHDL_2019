--
--  File Name:         Tb_Scoreboard1.vhd
--  Design Unit Name:  Tb_Scoreboard1
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis      jim@synthworks.com
--
--
--  Description:
--      VHDL-2019 Arrays of Protected Types
--
--
--  Developed by:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    04/2021   2021.04    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  
library ieee ;
  use ieee.std_logic_1164.all ;
  use ieee.numeric_std.all ;
  use ieee.numeric_std_unsigned.all ;
  use ieee.math_real.all ;
  
  use std.env.all ;

library osvvm ;
  context osvvm.OsvvmContext ;
    
  use work.ScoreboardStoreGenericPkg.all ; 
        
entity Tb_Scoreboard1 is
end entity Tb_Scoreboard1 ; 
architecture Test of Tb_Scoreboard1 is

  signal ScoreboardID1 : ScoreboardStoreIDType := NewScoreboard("Individual Scoreboard 1") ;
     
begin

  testProc : process
--    variable NameID2 : NameStoreIDType := NewScoreboard("String 2") ;
    variable ScoreboardID2 : ScoreboardStoreIDType := NewScoreboard("Individual Scoreboard 2") ;
    variable ScoreboardIDArray : ScoreboardStoreIDArrayType(1 to 10) ;
  begin 
    SetLogEnable(PASSED, TRUE) ; 

--    ScoreboardID1 <= NewScoreboard("Individual Scoreboard 1") ;
--
--    wait for 1 ns ; 
--    
--    ScoreboardID2 := NewScoreboard("Individual Scoreboard 2") ;

    log("Test 1: Checking 10 items in ScoreboardID1") ; 
    for i in 1 to 10 loop 
      push(ScoreboardID1, to_slv(i, 10)) ; 
    end loop ; 
    
    for i in 1 to 10 loop
      check(ScoreboardID1, to_slv(i, 10)) ; 
    end loop ; 
        
    log("Test 2: Checking 10 items in ScoreboardID2") ; 
    for i in 1 to 10 loop 
      push(ScoreboardID2, to_slv(16 + i, 10)) ; 
    end loop ; 
    
    for i in 1 to 10 loop 
      check(ScoreboardID2, to_slv(16 + i, 10)) ; 
    end loop ; 
    
    log("Test 3: Checking 5 items in Array of Scoreboards") ; 
    for i in ScoreboardIDArray'range loop
      ScoreboardIDArray(i) := NewScoreboard("ArrayScoreboard " & To_String(i)) ;
    end loop ;
    
    for i in ScoreboardIDArray'range loop
      for j in 1 to 5 loop
        push(ScoreboardIDArray(i), to_slv(i*16+j, 10)) ; 
      end loop ; 
    end loop ;

    for i in ScoreboardIDArray'reverse_range loop
      for j in 1 to 5 loop
        check(ScoreboardIDArray(i), to_slv(i*16+j, 10)) ; 
      end loop ; 
    end loop ;
    
    log("Test 4: Retest with ScoreboardID1 and ScoreboardID2") ; 
    for i in 1 to 5 loop 
      push(ScoreboardID1, to_slv(i, 10)) ; 
      push(ScoreboardID2, to_slv(16 + i, 10)) ; 
    end loop ; 
    
    for i in 1 to 5 loop
      check(ScoreboardID2, to_slv(16 + i, 10)) ; 
      check(ScoreboardID1, to_slv(i, 10)) ; 
    end loop ; 
        

    wait for 1 ns ; 
    ReportAlerts ; 
    std.env.stop(GetAlertCount) ; 
    wait ; 



    wait ; 
  end process testProc ; 
  

end architecture Test ;