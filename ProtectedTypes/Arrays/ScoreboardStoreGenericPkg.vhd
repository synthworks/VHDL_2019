--
--  File Name:         ScoreboardStoreGenericPkg.vhd
--  Design Unit Name:  ScoreboardStoreGenericPkg
--  Revision:          STANDARD VERSION
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis          email:  jim@synthworks.com
--
--
--  Description:
--    Implements Scoreboard Generic
--
--  Developed for:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        11898 SW 128th Ave.  Tigard, Or  97223
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version      Description
--    05/2021   2021.05     Initial revision
--
--
--  This file is part of OSVVM.
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  


use std.textio.all ;

library ieee ;
  use ieee.std_logic_1164.all ;
  use ieee.numeric_std.all ;
  
library osvvm ; 
  context osvvm.OsvvmContext ; 
  use osvvm.ScoreboardPkg_slv.all ;
  
package ScoreboardStoreGenericPkg is

  subtype ExpectedType is std_logic_vector ;
  subtype ActualType is std_logic_vector ;
  
  type ScoreboardStoreIDType is record
    ID : integer ;
  end record ScoreboardStoreIDType ; 
  type ScoreboardStoreIDArrayType is array (integer range <>) of ScoreboardStoreIDType ;  

  ------------------------------------------------------------
  impure function NewScoreboard (Name : String) return ScoreboardStoreIDType ;

  ------------------------------------------------------------
  -- Push items into the scoreboard/FIFO

  -- Simple Scoreboard, no tag
  procedure Push (
    constant ID     : in  ScoreboardStoreIDType ;
    constant Item   : in  ExpectedType
  ) ;
  
  -- Simple Tagged Scoreboard
  procedure Push (
    constant ID     : in  ScoreboardStoreIDType ;
    constant Tag    : in  string ;
    constant Item   : in  ExpectedType
  ) ;
  
  ------------------------------------------------------------
  -- Check received item with item in the scoreboard/FIFO
  
  -- Simple Scoreboard, no tag
  procedure Check (
    constant ID           : in  ScoreboardStoreIDType ;
    constant ActualData   : in ActualType
  ) ;
  
  -- Simple Tagged Scoreboard
  procedure Check (
    constant ID           : in  ScoreboardStoreIDType ;
    constant Tag          : in  string ;
    constant ActualData   : in  ActualType
  ) ;
  
  -- Simple Scoreboard, no tag
  impure function Check (
    constant ID           : in  ScoreboardStoreIDType ;
    constant ActualData   : in ActualType
  ) return boolean ; 
  
  -- Simple Tagged Scoreboard
  impure function Check (
    constant ID           : in  ScoreboardStoreIDType ;
    constant Tag          : in  string ;
    constant ActualData   : in  ActualType
  ) return boolean ;


  ------------------------------------------------------------
  -- Pop the top item (FIFO) from the scoreboard/FIFO
  
  -- Simple Scoreboard, no tag
  procedure Pop (
    constant ID     : in  ScoreboardStoreIDType ;
    variable Item   : out  ExpectedType
  ) ;
  
  -- Simple Tagged Scoreboard
  procedure Pop (
    constant ID     : in  ScoreboardStoreIDType ;
    constant Tag    : in  string ;
    variable Item   : out  ExpectedType
  ) ;


  ------------------------------------------------------------
  -- Pop the top item (FIFO) from the scoreboard/FIFO
  -- Caution:  this did not work in older simulators (@2013)

  -- Simple Scoreboard, no tag
  impure function Pop (
    constant ID     : in  ScoreboardStoreIDType 
  ) return ExpectedType ;
  
  -- Simple Tagged Scoreboard
  impure function Pop (
    constant ID     : in  ScoreboardStoreIDType ;
    constant Tag    : in  string       
  ) return ExpectedType ;


  ------------------------------------------------------------
  -- Peek at the top item (FIFO) from the scoreboard/FIFO
  
  -- Simple Tagged Scoreboard
  procedure Peek (
    constant ID     : in  ScoreboardStoreIDType ;
    constant Tag    : in  string ;
    variable Item   : out ExpectedType
  ) ;

  -- Simple Scoreboard, no tag
  procedure Peek (
    constant ID     : in  ScoreboardStoreIDType ;
    variable Item   : out  ExpectedType
  ) ;
  
  ------------------------------------------------------------
  -- Peek at the top item (FIFO) from the scoreboard/FIFO
  -- Caution:  this did not work in older simulators (@2013)
  
  -- Tagged Scoreboards
  impure function Peek (
    constant ID     : in  ScoreboardStoreIDType ;
    constant Tag    : in  string       
  ) return ExpectedType ;

  -- Simple Scoreboard
  impure function Peek (
    constant ID     : in  ScoreboardStoreIDType 
  ) return ExpectedType ;
  
  ------------------------------------------------------------
  -- Empty - check to see if scoreboard is empty
  -- Simple 
  impure function Empty (
    constant ID     : in  ScoreboardStoreIDType 
  ) return boolean ; 
  -- Tagged 
  impure function Empty (
    constant ID     : in  ScoreboardStoreIDType ;
    constant Tag    : in  string       
  ) return boolean ;                    -- Simple, Tagged

  ------------------------------------------------------------
  -- SetAlertLogID - associate an AlertLogID with a scoreboard to allow integrated error reporting
  procedure SetAlertLogID(
    constant ID              : in  ScoreboardStoreIDType ;
    constant Name            : in  string ; 
    constant ParentID        : in  AlertLogIDType := ALERTLOG_BASE_ID ; 
    constant CreateHierarchy : in  Boolean := TRUE
  ) ;

  -- Use when an AlertLogID is used by multiple items (Model or other Scoreboards).  See also AlertLogPkg.GetAlertLogID
  procedure SetAlertLogID (
    constant ID     : in  ScoreboardStoreIDType ;
    constant A      : AlertLogIDType
  ) ; 
    
  impure function GetAlertLogID (
    constant ID     : in  ScoreboardStoreIDType 
  ) return AlertLogIDType ;
  

  ------------------------------------------------------------
  -- Scoreboard Introspection  
  
  -- Number of items put into scoreboard
  impure function GetItemCount (
    constant ID     : in  ScoreboardStoreIDType 
  ) return integer ;   -- Simple, with or without tags

  impure function GetPushCount (
    constant ID     : in  ScoreboardStoreIDType 
  ) return integer ;   -- Simple, with or without tags
  
  -- Number of items removed from scoreboard by pop or check
  impure function GetPopCount (
    constant ID     : in  ScoreboardStoreIDType 
  ) return integer ;

  -- Number of items currently in the scoreboard (= PushCount - PopCount - DropCount)
  impure function GetFifoCount (
    constant ID     : in  ScoreboardStoreIDType 
  ) return integer ;

  -- Number of items checked by scoreboard
  impure function GetCheckCount (
    constant ID     : in  ScoreboardStoreIDType 
  ) return integer ;  -- Simple, with or without tags
  
  -- Number of items dropped by scoreboard.  See Find/Flush
  impure function GetDropCount (
    constant ID     : in  ScoreboardStoreIDType 
  ) return integer ;   -- Simple, with or without tags

  ------------------------------------------------------------
  -- Find - Returns the ItemNumber for a value and tag (if applicable) in a scoreboard. 
  -- Find returns integer'left if no match found
  -- Also See Flush.  Flush will drop items up through the ItemNumber
  
  -- Simple Scoreboard
  impure function Find (
    constant ID          : in  ScoreboardStoreIDType ;
    constant ActualData  :  in  ActualType 
  ) return integer ; 

  -- Tagged Scoreboard
  impure function Find (
    constant ID          : in  ScoreboardStoreIDType ;
    constant Tag         :  in  string; 
    constant ActualData  :  in  ActualType 
  ) return integer ; 
  
  ------------------------------------------------------------
  -- Flush - Remove elements in the scoreboard upto and including the one with ItemNumber
  -- See Find to identify an ItemNumber of a particular value and tag (if applicable)
  
  -- Simple Scoreboards
  procedure Flush (
    constant ID          : in  ScoreboardStoreIDType ;
    constant ItemNumber  :  in  integer 
  ) ; 

  -- Tagged Scoreboards - only removes items that also match the tag
  procedure Flush (
    constant ID          : in  ScoreboardStoreIDType ;
    constant Tag         :  in  string ; 
    constant ItemNumber  :  in  integer 
  ) ; 
  
  ------------------------------------------------------------
  -- Generally these are not required.  When a simulation ends and 
  -- another simulation is started, a simulator will release all allocated items.  
  procedure Deallocate (
    constant ID     : in  ScoreboardStoreIDType 
  ) ;  -- Deletes all allocated items
  procedure Initialize (
    constant ID     : in  ScoreboardStoreIDType 
  ) ;  -- Creates initial data structure if it was destroyed with Deallocate 
      
  ------------------------------------------------------------
  -- Get error count
  -- Deprecated, replaced by usage of Alerts
  -- AlertFLow:      Instead use AlertLogPkg.ReportAlerts or AlertLogPkg.GetAlertCount
  -- Not AlertFlow:  use GetErrorCount to get total error count 
  
  -- Scoreboards, with or without tag
  impure function GetErrorCount(
    constant ID     : in  ScoreboardStoreIDType 
  ) return integer ;
  
  ------------------------------------------------------------
  -- SetReportMode  
  -- Not AlertFlow
  --     REPORT_ALL:     Replaced by AlertLogPkg.SetLogEnable(PASSED, TRUE)
  --     REPORT_ERROR:   Replaced by AlertLogPkg.SetLogEnable(PASSED, FALSE)
  --     REPORT_NONE:    Deprecated, do not use.
  -- AlertFlow:      
  --     REPORT_ALL:     Replaced by AlertLogPkg.SetLogEnable(AlertLogID, PASSED, TRUE)
  --     REPORT_ERROR:   Replaced by AlertLogPkg.SetLogEnable(AlertLogID, PASSED, FALSE)
  --     REPORT_NONE:    Replaced by AlertLogPkg.SetAlertEnable(AlertLogID, ERROR, FALSE)
  procedure SetReportMode (
    constant ID           : in  ScoreboardStoreIDType ;
    constant ReportModeIn : in  ScoreboardReportType
  ) ;
  impure function GetReportMode (
    constant ID           : in  ScoreboardStoreIDType
    ) return ScoreboardReportType ;

  
  type ScoreboardStorePType is protected

    impure function NewScoreboard (Name : String) return ScoreboardStoreIDType ;

    ------------------------------------------------------------
    -- Push items into the scoreboard/FIFO

    -- Simple Scoreboard, no tag
    procedure Push (
      constant ID     : in  ScoreboardStoreIDType ;
      constant Item   : in  ExpectedType
    ) ;
    
    -- Simple Tagged Scoreboard
    procedure Push (
      constant ID     : in  ScoreboardStoreIDType ;
      constant Tag    : in  string ;
      constant Item   : in  ExpectedType
    ) ;
    
    ------------------------------------------------------------
    -- Check received item with item in the scoreboard/FIFO
    
    -- Simple Scoreboard, no tag
    procedure Check (
      constant ID           : in  ScoreboardStoreIDType ;
      constant ActualData   : in ActualType
    ) ;
    
    -- Simple Tagged Scoreboard
    procedure Check (
      constant ID           : in  ScoreboardStoreIDType ;
      constant Tag          : in  string ;
      constant ActualData   : in  ActualType
    ) ;
    
    -- Simple Scoreboard, no tag
    impure function Check (
      constant ID           : in  ScoreboardStoreIDType ;
      constant ActualData   : in ActualType
    ) return boolean ; 
    
    -- Simple Tagged Scoreboard
    impure function Check (
      constant ID           : in  ScoreboardStoreIDType ;
      constant Tag          : in  string ;
      constant ActualData   : in  ActualType
    ) return boolean ;


    ------------------------------------------------------------
    -- Pop the top item (FIFO) from the scoreboard/FIFO
    
    -- Simple Scoreboard, no tag
    procedure Pop (
      constant ID     : in  ScoreboardStoreIDType ;
      variable Item   : out  ExpectedType
    ) ;
    
    -- Simple Tagged Scoreboard
    procedure Pop (
      constant ID     : in  ScoreboardStoreIDType ;
      constant Tag    : in  string ;
      variable Item   : out  ExpectedType
    ) ;


    ------------------------------------------------------------
    -- Pop the top item (FIFO) from the scoreboard/FIFO
    -- Caution:  this did not work in older simulators (@2013)

    -- Simple Scoreboard, no tag
    impure function Pop (
      constant ID     : in  ScoreboardStoreIDType 
    ) return ExpectedType ;
    
    -- Simple Tagged Scoreboard
    impure function Pop (
      constant ID     : in  ScoreboardStoreIDType ;
      constant Tag    : in  string       
    ) return ExpectedType ;


    ------------------------------------------------------------
    -- Peek at the top item (FIFO) from the scoreboard/FIFO
    
    -- Simple Tagged Scoreboard
    procedure Peek (
      constant ID     : in  ScoreboardStoreIDType ;
      constant Tag    : in  string ;
      variable Item   : out ExpectedType
    ) ;

    -- Simple Scoreboard, no tag
    procedure Peek (
      constant ID     : in  ScoreboardStoreIDType ;
      variable Item   : out  ExpectedType
    ) ;
    
    ------------------------------------------------------------
    -- Peek at the top item (FIFO) from the scoreboard/FIFO
    -- Caution:  this did not work in older simulators (@2013)
    
    -- Tagged Scoreboards
    impure function Peek (
      constant ID     : in  ScoreboardStoreIDType ;
      constant Tag    : in  string       
    ) return ExpectedType ;

    -- Simple Scoreboard
    impure function Peek (
      constant ID     : in  ScoreboardStoreIDType 
    ) return ExpectedType ;
    
    ------------------------------------------------------------
    -- Empty - check to see if scoreboard is empty
    -- Simple 
    impure function Empty (
      constant ID     : in  ScoreboardStoreIDType 
    ) return boolean ; 
    -- Tagged 
    impure function Empty (
      constant ID     : in  ScoreboardStoreIDType ;
      constant Tag    : in  string       
    ) return boolean ;                    -- Simple, Tagged

    ------------------------------------------------------------
    -- SetAlertLogID - associate an AlertLogID with a scoreboard to allow integrated error reporting
    procedure SetAlertLogID(
      constant ID              : in  ScoreboardStoreIDType ;
      constant Name            : in  string ; 
      constant ParentID        : in  AlertLogIDType := ALERTLOG_BASE_ID ; 
      constant CreateHierarchy : in  Boolean := TRUE
    ) ;

    -- Use when an AlertLogID is used by multiple items (Model or other Scoreboards).  See also AlertLogPkg.GetAlertLogID
    procedure SetAlertLogID (
      constant ID     : in  ScoreboardStoreIDType ;
      constant A      : AlertLogIDType
    ) ; 
      
    impure function GetAlertLogID (
      constant ID     : in  ScoreboardStoreIDType 
    ) return AlertLogIDType ;
    

    ------------------------------------------------------------
    -- Scoreboard Introspection  
    
    -- Number of items put into scoreboard
    impure function GetItemCount (
      constant ID     : in  ScoreboardStoreIDType 
    ) return integer ;   -- Simple, with or without tags

    impure function GetPushCount (
      constant ID     : in  ScoreboardStoreIDType 
    ) return integer ;   -- Simple, with or without tags
    
    -- Number of items removed from scoreboard by pop or check
    impure function GetPopCount (
      constant ID     : in  ScoreboardStoreIDType 
    ) return integer ;

    -- Number of items currently in the scoreboard (= PushCount - PopCount - DropCount)
    impure function GetFifoCount (
      constant ID     : in  ScoreboardStoreIDType 
    ) return integer ;

    -- Number of items checked by scoreboard
    impure function GetCheckCount (
      constant ID     : in  ScoreboardStoreIDType 
    ) return integer ;  -- Simple, with or without tags
    
    -- Number of items dropped by scoreboard.  See Find/Flush
    impure function GetDropCount (
      constant ID     : in  ScoreboardStoreIDType 
    ) return integer ;   -- Simple, with or without tags

    ------------------------------------------------------------
    -- Find - Returns the ItemNumber for a value and tag (if applicable) in a scoreboard. 
    -- Find returns integer'left if no match found
    -- Also See Flush.  Flush will drop items up through the ItemNumber
    
    -- Simple Scoreboard
    impure function Find (
      constant ID          : in  ScoreboardStoreIDType ;
      constant ActualData  :  in  ActualType 
    ) return integer ; 

    -- Tagged Scoreboard
    impure function Find (
      constant ID          : in  ScoreboardStoreIDType ;
      constant Tag         :  in  string; 
      constant ActualData  :  in  ActualType 
    ) return integer ; 
    
    ------------------------------------------------------------
    -- Flush - Remove elements in the scoreboard upto and including the one with ItemNumber
    -- See Find to identify an ItemNumber of a particular value and tag (if applicable)
    
    -- Simple Scoreboards
    procedure Flush (
      constant ID          : in  ScoreboardStoreIDType ;
      constant ItemNumber  :  in  integer 
    ) ; 

    -- Tagged Scoreboards - only removes items that also match the tag
    procedure Flush (
      constant ID          : in  ScoreboardStoreIDType ;
      constant Tag         :  in  string ; 
      constant ItemNumber  :  in  integer 
    ) ; 
    
    ------------------------------------------------------------
    -- Generally these are not required.  When a simulation ends and 
    -- another simulation is started, a simulator will release all allocated items.  
    procedure Deallocate (
      constant ID     : in  ScoreboardStoreIDType 
    ) ;  -- Deletes all allocated items
    procedure Initialize (
      constant ID     : in  ScoreboardStoreIDType 
    ) ;  -- Creates initial data structure if it was destroyed with Deallocate 
        
    ------------------------------------------------------------
    -- Get error count
    -- Deprecated, replaced by usage of Alerts
    -- AlertFLow:      Instead use AlertLogPkg.ReportAlerts or AlertLogPkg.GetAlertCount
    -- Not AlertFlow:  use GetErrorCount to get total error count 
    
    -- Scoreboards, with or without tag
    impure function GetErrorCount(
      constant ID     : in  ScoreboardStoreIDType 
    ) return integer ;
    
    ------------------------------------------------------------
    -- SetReportMode  
    -- Not AlertFlow
    --     REPORT_ALL:     Replaced by AlertLogPkg.SetLogEnable(PASSED, TRUE)
    --     REPORT_ERROR:   Replaced by AlertLogPkg.SetLogEnable(PASSED, FALSE)
    --     REPORT_NONE:    Deprecated, do not use.
    -- AlertFlow:      
    --     REPORT_ALL:     Replaced by AlertLogPkg.SetLogEnable(AlertLogID, PASSED, TRUE)
    --     REPORT_ERROR:   Replaced by AlertLogPkg.SetLogEnable(AlertLogID, PASSED, FALSE)
    --     REPORT_NONE:    Replaced by AlertLogPkg.SetAlertEnable(AlertLogID, ERROR, FALSE)
    procedure SetReportMode (
      constant ID           : in  ScoreboardStoreIDType ;
      constant ReportModeIn : in  ScoreboardReportType
    ) ;
    impure function GetReportMode (
      constant ID           : in  ScoreboardStoreIDType
    ) return ScoreboardReportType ;

  end protected ScoreboardStorePType ;

end ScoreboardStoreGenericPkg ;


-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
package body ScoreboardStoreGenericPkg is

  type ScoreboardStorePType is protected body
  
--    alias ItemType         is osvvm.ScoreboardPkg_slv.ScoreboardPType ; 
    alias ItemType         is ScoreboardPType ; 
    type  ItemPtrType      is access ItemType ; 
    type  ItemArrayType    is array (integer range <>) of ItemPtrType ;  
    type  ItemArrayPtrType is access ItemArrayType ;
	
    variable ScoreboardArrayPtr : ItemArrayPtrType ;
    variable NumItems       : integer := 0 ; 
    constant MIN_NUM_ITEMS  : integer := 4 ; -- Temporarily small for testing
--    constant MIN_NUM_ITEMS  : integer := 32 ; -- Min amount to resize array

    ------------------------------------------------------------
    -- Package Local
    function NormalizeArraySize( NewNumItems, MinNumItems : integer ) return integer is
    ------------------------------------------------------------
      variable NormNumItems : integer := NewNumItems ;
      variable ModNumItems  : integer := 0;
    begin
      ModNumItems := NewNumItems mod MinNumItems ; 
      if ModNumItems > 0 then 
        NormNumItems := NormNumItems + (MinNumItems - ModNumItems) ; 
      end if ; 
      return NormNumItems ; 
    end function NormalizeArraySize ;

    ------------------------------------------------------------
    -- Package Local
    procedure GrowNumberItems (
    ------------------------------------------------------------
      variable ItemArrayPtr     : InOut ItemArrayPtrType ;
      constant NewNumItems      : in integer ;
      constant CurNumItems      : in integer ;
      constant MinNumItems      : in integer 
    ) is
      variable oldItemArrayPtr  : ItemArrayPtrType ;
    begin
      if ItemArrayPtr = NULL then
        ItemArrayPtr := new ItemArrayType(1 to NormalizeArraySize(NewNumItems, MinNumItems)) ;
      elsif NewNumItems > ItemArrayPtr'length then
        oldItemArrayPtr := ItemArrayPtr ;
        ItemArrayPtr := new ItemArrayType(1 to NormalizeArraySize(NewNumItems, MinNumItems)) ;
        ItemArrayPtr.all(1 to CurNumItems) := oldItemArrayPtr.all(1 to CurNumItems) ;
        deallocate(oldItemArrayPtr) ;
      end if ;
      for i in CurNumItems + 1 to NewNumItems loop
        ItemArrayPtr(i) := new ItemType ;
      end loop ; 
    end procedure GrowNumberItems ;

    ------------------------------------------------------------
    impure function NewScoreboard (Name : String) return ScoreboardStoreIDType is
    ------------------------------------------------------------
      variable Result : ScoreboardStoreIDType ; 
      variable NewNumItems : integer ;
    begin
      NewNumItems := NumItems + 1 ; 
      GrowNumberItems(ScoreboardArrayPtr, NewNumItems, NumItems, MIN_NUM_ITEMS) ;
      Result.ID := NewNumItems ; 
      NumItems  := NewNumItems ;
      ScoreboardArrayPtr(Result.ID).SetAlertLogID(Name) ; 
--      Set(Result, NameIn) ; 
      return Result ; 
    end function NewScoreboard ;

    ------------------------------------------------------------
    -- Push items into the scoreboard/FIFO

    ------------------------------------------------------------
    -- Simple Scoreboard, no tag
    procedure Push (
    ------------------------------------------------------------
      constant ID     : in  ScoreboardStoreIDType ;
      constant Item   : in  ExpectedType
    ) is
    begin
      ScoreboardArrayPtr(ID.ID).Push(Item) ; 
    end procedure Push ; 
    
    -- Simple Tagged Scoreboard
    procedure Push (
      constant ID     : in  ScoreboardStoreIDType ;
      constant Tag    : in  string ;
      constant Item   : in  ExpectedType
    ) is
    begin
      ScoreboardArrayPtr(ID.ID).Push(Tag, Item) ; 
    end procedure Push ; 
    
    ------------------------------------------------------------
    -- Check received item with item in the scoreboard/FIFO
    
    -- Simple Scoreboard, no tag
    procedure Check (
      constant ID           : in  ScoreboardStoreIDType ;
      constant ActualData   : in ActualType
    ) is
    begin
      ScoreboardArrayPtr(ID.ID).Check(ActualData) ; 
    end procedure Check ; 
    
    -- Simple Tagged Scoreboard
    procedure Check (
      constant ID           : in  ScoreboardStoreIDType ;
      constant Tag          : in  string ;
      constant ActualData   : in  ActualType
    ) is
    begin
      ScoreboardArrayPtr(ID.ID).Check(Tag, ActualData) ; 
    end procedure Check ;
    
    -- Simple Scoreboard, no tag
    impure function Check (
      constant ID           : in  ScoreboardStoreIDType ;
      constant ActualData   : in ActualType
    ) return boolean is
    begin
      return ScoreboardArrayPtr(ID.ID).Check(ActualData) ; 
    end function Check ; 
    
    -- Simple Tagged Scoreboard
    impure function Check (
      constant ID           : in  ScoreboardStoreIDType ;
      constant Tag          : in  string ;
      constant ActualData   : in  ActualType
    ) return boolean is
    begin
      return ScoreboardArrayPtr(ID.ID).Check(Tag, ActualData) ; 
    end function Check ;


    ------------------------------------------------------------
    -- Pop the top item (FIFO) from the scoreboard/FIFO
    
    -- Simple Scoreboard, no tag
    procedure Pop (
      constant ID     : in  ScoreboardStoreIDType ;
      variable Item   : out  ExpectedType
    ) is
    begin
      ScoreboardArrayPtr(ID.ID).Pop(Item) ; 
    end procedure Pop ;
    
    -- Simple Tagged Scoreboard
    procedure Pop (
      constant ID     : in  ScoreboardStoreIDType ;
      constant Tag    : in  string ;
      variable Item   : out  ExpectedType
    ) is
    begin
      ScoreboardArrayPtr(ID.ID).Pop(Tag, Item) ; 
    end procedure Pop ;


    ------------------------------------------------------------
    -- Pop the top item (FIFO) from the scoreboard/FIFO
    -- Caution:  this did not work in older simulators (@2013)

    -- Simple Scoreboard, no tag
    impure function Pop (
      constant ID     : in  ScoreboardStoreIDType 
    ) return ExpectedType is
    begin
      return ScoreboardArrayPtr(ID.ID).Pop ; 
    end function Pop ;
    
    -- Simple Tagged Scoreboard
    impure function Pop (
      constant ID     : in  ScoreboardStoreIDType ;
      constant Tag    : in  string       
    ) return ExpectedType is
    begin
      return ScoreboardArrayPtr(ID.ID).Pop(Tag) ; 
    end function Pop ;


    ------------------------------------------------------------
    -- Peek at the top item (FIFO) from the scoreboard/FIFO
    
    -- Simple Tagged Scoreboard
    procedure Peek (
      constant ID     : in  ScoreboardStoreIDType ;
      constant Tag    : in  string ;
      variable Item   : out ExpectedType
    ) is
    begin
      ScoreboardArrayPtr(ID.ID).Peek(Tag, Item) ; 
    end procedure Peek ;

    -- Simple Scoreboard, no tag
    procedure Peek (
      constant ID     : in  ScoreboardStoreIDType ;
      variable Item   : out  ExpectedType
    ) is
    begin
      ScoreboardArrayPtr(ID.ID).Peek(Item) ; 
    end procedure Peek ;
    
    ------------------------------------------------------------
    -- Peek at the top item (FIFO) from the scoreboard/FIFO
    -- Caution:  this did not work in older simulators (@2013)
    
    -- Tagged Scoreboards
    impure function Peek (
      constant ID     : in  ScoreboardStoreIDType ;
      constant Tag    : in  string       
    ) return ExpectedType is
    begin
--      return ScoreboardArrayPtr(ID.ID).Peek(Tag) ;
--      return ScoreboardArrayPtr(ID.ID).Peek(string'(Tag)) ; 
      log("Issues compiling return later");
      return ScoreboardArrayPtr(ID.ID).Peek ; 
    end function Peek ;

    -- Simple Scoreboard
    impure function Peek (
      constant ID     : in  ScoreboardStoreIDType 
    ) return ExpectedType is
    begin
      return ScoreboardArrayPtr(ID.ID).Peek ; 
    end function Peek ;
    
    ------------------------------------------------------------
    -- Empty - check to see if scoreboard is empty
    -- Simple 
    impure function Empty (
      constant ID     : in  ScoreboardStoreIDType 
    ) return boolean is
    begin
      return ScoreboardArrayPtr(ID.ID).Empty ; 
    end function Empty ;
    
    -- Tagged 
    impure function Empty (
      constant ID     : in  ScoreboardStoreIDType ;
      constant Tag    : in  string       
    ) return boolean is
    begin
      return ScoreboardArrayPtr(ID.ID).Empty(Tag) ; 
    end function Empty ;

    ------------------------------------------------------------
    -- SetAlertLogID - associate an AlertLogID with a scoreboard to allow integrated error reporting
    procedure SetAlertLogID(
      constant ID              : in  ScoreboardStoreIDType ;
      constant Name            : in  string ; 
      constant ParentID        : in  AlertLogIDType := ALERTLOG_BASE_ID ; 
      constant CreateHierarchy : in  Boolean := TRUE
    ) is
    begin
      ScoreboardArrayPtr(ID.ID).SetAlertLogID(Name, ParentID, CreateHierarchy) ; 
    end procedure SetAlertLogID ;

    -- Use when an AlertLogID is used by multiple items (Model or other Scoreboards).  See also AlertLogPkg.GetAlertLogID
    procedure SetAlertLogID (
      constant ID     : in  ScoreboardStoreIDType ;
      constant A      : AlertLogIDType
    ) is
    begin
      ScoreboardArrayPtr(ID.ID).SetAlertLogID(A) ; 
    end procedure SetAlertLogID ; 
      
    impure function GetAlertLogID (
      constant ID     : in  ScoreboardStoreIDType 
    ) return AlertLogIDType is
    begin
      return ScoreboardArrayPtr(ID.ID).GetAlertLogID ; 
    end function GetAlertLogID ;
    

    ------------------------------------------------------------
    -- Scoreboard Introspection  
    
    -- Number of items put into scoreboard
    impure function GetItemCount (
      constant ID     : in  ScoreboardStoreIDType 
    ) return integer is
    begin
      return ScoreboardArrayPtr(ID.ID).GetItemCount ; 
    end function GetItemCount ;

    impure function GetPushCount (
      constant ID     : in  ScoreboardStoreIDType 
    ) return integer is
    begin
      return ScoreboardArrayPtr(ID.ID).GetPushCount ; 
    end function GetPushCount ;
    
    -- Number of items removed from scoreboard by pop or check
    impure function GetPopCount (
      constant ID     : in  ScoreboardStoreIDType 
    ) return integer is
    begin
      return ScoreboardArrayPtr(ID.ID).GetPopCount ; 
    end function GetPopCount ;

    -- Number of items currently in the scoreboard (= PushCount - PopCount - DropCount)
    impure function GetFifoCount (
      constant ID     : in  ScoreboardStoreIDType 
    ) return integer is
    begin
      return ScoreboardArrayPtr(ID.ID).GetFifoCount ; 
    end function GetFifoCount ;

    -- Number of items checked by scoreboard
    impure function GetCheckCount (
      constant ID     : in  ScoreboardStoreIDType 
    ) return integer is
    begin
      return ScoreboardArrayPtr(ID.ID).GetCheckCount ; 
    end function GetCheckCount ;
    
    -- Number of items dropped by scoreboard.  See Find/Flush
    impure function GetDropCount (
      constant ID     : in  ScoreboardStoreIDType 
    ) return integer is
    begin
      return ScoreboardArrayPtr(ID.ID).GetDropCount ; 
    end function GetDropCount ;

    ------------------------------------------------------------
    -- Find - Returns the ItemNumber for a value and tag (if applicable) in a scoreboard. 
    -- Find returns integer'left if no match found
    -- Also See Flush.  Flush will drop items up through the ItemNumber
    
    -- Simple Scoreboard
    impure function Find (
      constant ID          : in  ScoreboardStoreIDType ;
      constant ActualData  : in  ActualType 
    ) return integer is
    begin
      return ScoreboardArrayPtr(ID.ID).Find(ActualData) ; 
    end function Find ; 

    -- Tagged Scoreboard
    impure function Find (
      constant ID          : in  ScoreboardStoreIDType ;
      constant Tag         : in  string; 
      constant ActualData  : in  ActualType 
    ) return integer is
    begin
      return ScoreboardArrayPtr(ID.ID).Find(Tag, ActualData) ; 
    end function Find ; 
    
    ------------------------------------------------------------
    -- Flush - Remove elements in the scoreboard upto and including the one with ItemNumber
    -- See Find to identify an ItemNumber of a particular value and tag (if applicable)
    
    -- Simple Scoreboards
    procedure Flush (
      constant ID          : in  ScoreboardStoreIDType ;
      constant ItemNumber  :  in  integer 
    ) is
    begin
      ScoreboardArrayPtr(ID.ID).Flush(ItemNumber) ; 
    end procedure Flush ;
    

    -- Tagged Scoreboards - only removes items that also match the tag
    procedure Flush (
      constant ID          : in  ScoreboardStoreIDType ;
      constant Tag         :  in  string ; 
      constant ItemNumber  :  in  integer 
    ) is
    begin
      ScoreboardArrayPtr(ID.ID).Flush(Tag, ItemNumber) ; 
    end procedure Flush ; 
    
    ------------------------------------------------------------
    -- Generally these are not required.  When a simulation ends and 
    -- another simulation is started, a simulator will release all allocated items.  
    procedure Deallocate (
      constant ID     : in  ScoreboardStoreIDType 
    ) is
    begin
      ScoreboardArrayPtr(ID.ID).Deallocate ; 
    end procedure Deallocate ; 
    
    procedure Initialize (
      constant ID     : in  ScoreboardStoreIDType 
    ) is
    begin
      ScoreboardArrayPtr(ID.ID).Initialize ; 
    end procedure Initialize ; 
        
    ------------------------------------------------------------
    -- Get error count
    -- Deprecated, replaced by usage of Alerts
    -- AlertFLow:      Instead use AlertLogPkg.ReportAlerts or AlertLogPkg.GetAlertCount
    -- Not AlertFlow:  use GetErrorCount to get total error count 
    
    -- Scoreboards, with or without tag
    impure function GetErrorCount(
      constant ID     : in  ScoreboardStoreIDType 
    ) return integer is
    begin
      return ScoreboardArrayPtr(ID.ID).GetErrorCount ; 
    end function GetErrorCount ; 
    
    ------------------------------------------------------------
    -- SetReportMode  
    -- Not AlertFlow
    --     REPORT_ALL:     Replaced by AlertLogPkg.SetLogEnable(PASSED, TRUE)
    --     REPORT_ERROR:   Replaced by AlertLogPkg.SetLogEnable(PASSED, FALSE)
    --     REPORT_NONE:    Deprecated, do not use.
    -- AlertFlow:      
    --     REPORT_ALL:     Replaced by AlertLogPkg.SetLogEnable(AlertLogID, PASSED, TRUE)
    --     REPORT_ERROR:   Replaced by AlertLogPkg.SetLogEnable(AlertLogID, PASSED, FALSE)
    --     REPORT_NONE:    Replaced by AlertLogPkg.SetAlertEnable(AlertLogID, ERROR, FALSE)
    procedure SetReportMode (
      constant ID           : in  ScoreboardStoreIDType ;
      constant ReportModeIn : in  ScoreboardReportType
    ) is
    begin
      ScoreboardArrayPtr(ID.ID).SetReportMode(ReportModeIn) ; 
    end procedure SetReportMode ; 
    
    impure function GetReportMode (
      constant ID           : in  ScoreboardStoreIDType
    ) return ScoreboardReportType is
    begin
      return ScoreboardArrayPtr(ID.ID).GetReportMode ; 
    end function GetReportMode ; 

  end protected body ScoreboardStorePType ;
  
  variable ScoreboardStore : ScoreboardStorePType ;
  
  ------------------------------------------------------------
  impure function NewScoreboard (Name : String) return ScoreboardStoreIDType is
  ------------------------------------------------------------
  begin
    return ScoreboardStore.NewScoreboard(Name) ; 
  end function NewScoreboard ;

  ------------------------------------------------------------
  -- Push items into the scoreboard/FIFO

  ------------------------------------------------------------
  -- Simple Scoreboard, no tag
  procedure Push (
  ------------------------------------------------------------
    constant ID     : in  ScoreboardStoreIDType ;
    constant Item   : in  ExpectedType
  ) is
  begin
    ScoreboardStore.Push(ID, Item) ; 
  end procedure Push ; 
  
  -- Simple Tagged Scoreboard
  procedure Push (
    constant ID     : in  ScoreboardStoreIDType ;
    constant Tag    : in  string ;
    constant Item   : in  ExpectedType
  ) is
  begin
    ScoreboardStore.Push(ID, Tag, Item) ; 
  end procedure Push ; 
  
  ------------------------------------------------------------
  -- Check received item with item in the scoreboard/FIFO
  
  -- Simple Scoreboard, no tag
  procedure Check (
    constant ID           : in  ScoreboardStoreIDType ;
    constant ActualData   : in ActualType
  ) is
  begin
    ScoreboardStore.Check(ID, ActualData) ; 
  end procedure Check ; 
  
  -- Simple Tagged Scoreboard
  procedure Check (
    constant ID           : in  ScoreboardStoreIDType ;
    constant Tag          : in  string ;
    constant ActualData   : in  ActualType
  ) is
  begin
    ScoreboardStore.Check(ID, Tag, ActualData) ; 
  end procedure Check ;
  
  -- Simple Scoreboard, no tag
  impure function Check (
    constant ID           : in  ScoreboardStoreIDType ;
    constant ActualData   : in ActualType
  ) return boolean is
  begin
    return ScoreboardStore.Check(ID, ActualData) ; 
  end function Check ; 
  
  -- Simple Tagged Scoreboard
  impure function Check (
    constant ID           : in  ScoreboardStoreIDType ;
    constant Tag          : in  string ;
    constant ActualData   : in  ActualType
  ) return boolean is
  begin
    return ScoreboardStore.Check(ID, Tag, ActualData) ; 
  end function Check ;


  ------------------------------------------------------------
  -- Pop the top item (FIFO) from the scoreboard/FIFO
  
  -- Simple Scoreboard, no tag
  procedure Pop (
    constant ID     : in  ScoreboardStoreIDType ;
    variable Item   : out  ExpectedType
  ) is
  begin
    ScoreboardStore.Pop(ID, Item) ; 
  end procedure Pop ;
  
  -- Simple Tagged Scoreboard
  procedure Pop (
    constant ID     : in  ScoreboardStoreIDType ;
    constant Tag    : in  string ;
    variable Item   : out  ExpectedType
  ) is
  begin
    ScoreboardStore.Pop(ID, Tag, Item) ; 
  end procedure Pop ;


  ------------------------------------------------------------
  -- Pop the top item (FIFO) from the scoreboard/FIFO
  -- Caution:  this did not work in older simulators (@2013)

  -- Simple Scoreboard, no tag
  impure function Pop (
    constant ID     : in  ScoreboardStoreIDType 
  ) return ExpectedType is
  begin
    return ScoreboardStore.Pop(ID) ; 
  end function Pop ;
  
  -- Simple Tagged Scoreboard
  impure function Pop (
    constant ID     : in  ScoreboardStoreIDType ;
    constant Tag    : in  string       
  ) return ExpectedType is
  begin
    return ScoreboardStore.Pop(ID, Tag) ; 
  end function Pop ;


  ------------------------------------------------------------
  -- Peek at the top item (FIFO) from the scoreboard/FIFO
  
  -- Simple Tagged Scoreboard
  procedure Peek (
    constant ID     : in  ScoreboardStoreIDType ;
    constant Tag    : in  string ;
    variable Item   : out ExpectedType
  ) is
  begin
    ScoreboardStore.Peek(ID, Tag, Item) ; 
  end procedure Peek ;

  -- Simple Scoreboard, no tag
  procedure Peek (
    constant ID     : in  ScoreboardStoreIDType ;
    variable Item   : out  ExpectedType
  ) is
  begin
    ScoreboardStore.Peek(ID, Item) ; 
  end procedure Peek ;
  
  ------------------------------------------------------------
  -- Peek at the top item (FIFO) from the scoreboard/FIFO
  -- Caution:  this did not work in older simulators (@2013)
  
  -- Tagged Scoreboards
  impure function Peek (
    constant ID     : in  ScoreboardStoreIDType ;
    constant Tag    : in  string       
  ) return ExpectedType is
  begin
--      return ScoreboardStore.Peek(Tag) ;
    log("Issues compiling return later");
    return ScoreboardStore.Peek(ID) ; 
  end function Peek ;

  -- Simple Scoreboard
  impure function Peek (
    constant ID     : in  ScoreboardStoreIDType 
  ) return ExpectedType is
  begin
    return ScoreboardStore.Peek(ID) ; 
  end function Peek ;
  
  ------------------------------------------------------------
  -- Empty - check to see if scoreboard is empty
  -- Simple 
  impure function Empty (
    constant ID     : in  ScoreboardStoreIDType 
  ) return boolean is
  begin
    return ScoreboardStore.Empty(ID) ; 
  end function Empty ;
  
  -- Tagged 
  impure function Empty (
    constant ID     : in  ScoreboardStoreIDType ;
    constant Tag    : in  string       
  ) return boolean is
  begin
    return ScoreboardStore.Empty(ID, Tag) ; 
  end function Empty ;

  ------------------------------------------------------------
  -- SetAlertLogID - associate an AlertLogID with a scoreboard to allow integrated error reporting
  procedure SetAlertLogID(
    constant ID              : in  ScoreboardStoreIDType ;
    constant Name            : in  string ; 
    constant ParentID        : in  AlertLogIDType := ALERTLOG_BASE_ID ; 
    constant CreateHierarchy : in  Boolean := TRUE
  ) is
  begin
    ScoreboardStore.SetAlertLogID(ID, Name, ParentID, CreateHierarchy) ; 
  end procedure SetAlertLogID ;

  -- Use when an AlertLogID is used by multiple items (Model or other Scoreboards).  See also AlertLogPkg.GetAlertLogID
  procedure SetAlertLogID (
    constant ID     : in  ScoreboardStoreIDType ;
    constant A      : AlertLogIDType
  ) is
  begin
    ScoreboardStore.SetAlertLogID(ID, A) ; 
  end procedure SetAlertLogID ; 
    
  impure function GetAlertLogID (
    constant ID     : in  ScoreboardStoreIDType 
  ) return AlertLogIDType is
  begin
    return ScoreboardStore.GetAlertLogID(ID) ; 
  end function GetAlertLogID ;
  

  ------------------------------------------------------------
  -- Scoreboard Introspection  
  
  -- Number of items put into scoreboard
  impure function GetItemCount (
    constant ID     : in  ScoreboardStoreIDType 
  ) return integer is
  begin
    return ScoreboardStore.GetItemCount(ID) ; 
  end function GetItemCount ;

  impure function GetPushCount (
    constant ID     : in  ScoreboardStoreIDType 
  ) return integer is
  begin
    return ScoreboardStore.GetPushCount(ID) ; 
  end function GetPushCount ;
  
  -- Number of items removed from scoreboard by pop or check
  impure function GetPopCount (
    constant ID     : in  ScoreboardStoreIDType 
  ) return integer is
  begin
    return ScoreboardStore.GetPopCount(ID) ; 
  end function GetPopCount ;

  -- Number of items currently in the scoreboard (= PushCount - PopCount - DropCount)
  impure function GetFifoCount (
    constant ID     : in  ScoreboardStoreIDType 
  ) return integer is
  begin
    return ScoreboardStore.GetFifoCount(ID) ; 
  end function GetFifoCount ;

  -- Number of items checked by scoreboard
  impure function GetCheckCount (
    constant ID     : in  ScoreboardStoreIDType 
  ) return integer is
  begin
    return ScoreboardStore.GetCheckCount(ID) ; 
  end function GetCheckCount ;
  
  -- Number of items dropped by scoreboard.  See Find/Flush
  impure function GetDropCount (
    constant ID     : in  ScoreboardStoreIDType 
  ) return integer is
  begin
    return ScoreboardStore.GetDropCount(ID) ; 
  end function GetDropCount ;

  ------------------------------------------------------------
  -- Find - Returns the ItemNumber for a value and tag (if applicable) in a scoreboard. 
  -- Find returns integer'left if no match found
  -- Also See Flush.  Flush will drop items up through the ItemNumber
  
  -- Simple Scoreboard
  impure function Find (
    constant ID          : in  ScoreboardStoreIDType ;
    constant ActualData  : in  ActualType 
  ) return integer is
  begin
    return ScoreboardStore.Find(ID, ActualData) ; 
  end function Find ; 

  -- Tagged Scoreboard
  impure function Find (
    constant ID          : in  ScoreboardStoreIDType ;
    constant Tag         : in  string; 
    constant ActualData  : in  ActualType 
  ) return integer is
  begin
    return ScoreboardStore.Find(ID, Tag, ActualData) ; 
  end function Find ; 
  
  ------------------------------------------------------------
  -- Flush - Remove elements in the scoreboard upto and including the one with ItemNumber
  -- See Find to identify an ItemNumber of a particular value and tag (if applicable)
  
  -- Simple Scoreboards
  procedure Flush (
    constant ID          : in  ScoreboardStoreIDType ;
    constant ItemNumber  :  in  integer 
  ) is
  begin
    ScoreboardStore.Flush(ID, ItemNumber) ; 
  end procedure Flush ;
  

  -- Tagged Scoreboards - only removes items that also match the tag
  procedure Flush (
    constant ID          : in  ScoreboardStoreIDType ;
    constant Tag         :  in  string ; 
    constant ItemNumber  :  in  integer 
  ) is
  begin
    ScoreboardStore.Flush(ID, Tag, ItemNumber) ; 
  end procedure Flush ; 
  
  ------------------------------------------------------------
  -- Generally these are not required.  When a simulation ends and 
  -- another simulation is started, a simulator will release all allocated items.  
  procedure Deallocate (
    constant ID     : in  ScoreboardStoreIDType 
  ) is
  begin
    ScoreboardStore.Deallocate(ID) ; 
  end procedure Deallocate ; 
  
  procedure Initialize (
    constant ID     : in  ScoreboardStoreIDType 
  ) is
  begin
    ScoreboardStore.Initialize(ID) ; 
  end procedure Initialize ; 
      
  ------------------------------------------------------------
  -- Get error count
  -- Deprecated, replaced by usage of Alerts
  -- AlertFLow:      Instead use AlertLogPkg.ReportAlerts or AlertLogPkg.GetAlertCount
  -- Not AlertFlow:  use GetErrorCount to get total error count 
  
  -- Scoreboards, with or without tag
  impure function GetErrorCount(
    constant ID     : in  ScoreboardStoreIDType 
  ) return integer is
  begin
    return ScoreboardStore.GetErrorCount(ID) ; 
  end function GetErrorCount ; 
  
  ------------------------------------------------------------
  -- SetReportMode  
  -- Not AlertFlow
  --     REPORT_ALL:     Replaced by AlertLogPkg.SetLogEnable(PASSED, TRUE)
  --     REPORT_ERROR:   Replaced by AlertLogPkg.SetLogEnable(PASSED, FALSE)
  --     REPORT_NONE:    Deprecated, do not use.
  -- AlertFlow:      
  --     REPORT_ALL:     Replaced by AlertLogPkg.SetLogEnable(AlertLogID, PASSED, TRUE)
  --     REPORT_ERROR:   Replaced by AlertLogPkg.SetLogEnable(AlertLogID, PASSED, FALSE)
  --     REPORT_NONE:    Replaced by AlertLogPkg.SetAlertEnable(AlertLogID, ERROR, FALSE)
  procedure SetReportMode (
    constant ID           : in  ScoreboardStoreIDType ;
    constant ReportModeIn : in  ScoreboardReportType
  ) is
  begin
    ScoreboardStore.SetReportMode(ID, ReportModeIn) ; 
  end procedure SetReportMode ; 
  
  impure function GetReportMode (
    constant ID           : in  ScoreboardStoreIDType
  ) return ScoreboardReportType is
  begin
    return ScoreboardStore.GetReportMode(ID) ; 
  end function GetReportMode ; 
    
end ScoreboardStoreGenericPkg ;