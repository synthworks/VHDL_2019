--
--  File Name:         ArrayAllocationPkg.vhd
--  Design Unit Name:  ArrayAllocationPkg
--  Revision:          STANDARD VERSION
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis          SynthWorks
--
--
--  Package Defines
--      Sizes/Resizes arrays for NameStorePkg, ...
--
--  Developed for:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        11898 SW 128th Ave.  Tigard, Or  97223
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    04/2021:  2021.04    Initial revision
--
--
--  This file is part of OSVVM.
--  
--  Copyright (c) 2010 - 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  

library osvvm ; 
  context osvvm.OsvvmContext ; 
  
-- package ArrayAllocationGenericPkg is
--   generic ( type ItemType ) ; 
  
package ArrayAllocationPkg is

  alias ItemType is NamePType ; 

  type ItemPtrType is access ItemType ; 
  type ItemArrayType is array (integer range <>) of ItemPtrType ;  
  type ItemArrayPtrType is access ItemArrayType ;
  
  procedure GrowNumberItems (
    variable ItemArrayPtr     : InOut ItemArrayPtrType ;
    constant NewTotalNumItems : in integer ;
    constant MinNumItems      : in integer 
  ) ;

end package ArrayAllocationPkg ;

--- ///////////////////////////////////////////////////////////////////////////
--- ///////////////////////////////////////////////////////////////////////////
--- ///////////////////////////////////////////////////////////////////////////

package body ArrayAllocationPkg is
  

  -- Package Local
  function NormalizeArraySize( NewTotalNumItems, MinNumItems : integer ) return integer is
    variable NormNumItems : integer := NewTotalNumItems ;
    variable ModNumItems  : integer := 0;
  begin
    ModNumItems := NewTotalNumItems mod MinNumItems ; 
    if ModNumItems > 0 then 
      NormNumItems := NormNumItems + (MinNumItems - ModNumItems) ; 
    end if ; 
    
    return NormNumItems ; 
  end function NormalizeArraySize ;

  procedure GrowNumberItems (
    variable ItemArrayPtr     : InOut ItemArrayPtrType ;
    constant NewTotalNumItems : in integer ;
    constant MinNumItems      : in integer 
  ) is
    variable oldItemArrayPtr  : ItemArrayPtrType ;
    variable CurrentSize      : integer ; 
  begin
    if ItemArrayPtr = NULL then
      ItemArrayPtr := new ItemArrayType(1 to NormalizeArraySize(NewTotalNumItems, MinNumItems)) ;
      for i in 1 to ItemArrayPtr'length loop
        ItemArrayPtr(i) := new ItemType ;
      end loop ; 
    elsif NewTotalNumItems > ItemArrayPtr'length then
      CurrentSize := ItemArrayPtr'length ;
      oldItemArrayPtr := ItemArrayPtr ;
      ItemArrayPtr := new ItemArrayType(1 to NormalizeArraySize(NewTotalNumItems, MinNumItems)) ;
--      ItemArrayPtr.all(1 to oldItemArrayPtr'length) := oldItemArrayPtr.all(1 to oldItemArrayPtr'length) ;
      ItemArrayPtr.all(1 to CurrentSize) := oldItemArrayPtr.all(1 to CurrentSize) ;
      deallocate(oldItemArrayPtr) ;
      for i in CurrentSize + 1 to ItemArrayPtr'length loop
        ItemArrayPtr(i) := new ItemType ;
      end loop ; 
    end if ;
  end procedure GrowNumberItems ;


end package body ArrayAllocationPkg ;