--
--  File Name:         NameStorePkg.vhd
--  Design Unit Name:  NameStorePkg
--  Revision:          STANDARD VERSION
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis          SynthWorks
--
--
--  Package Defines
--      Data structure for name. 
--
--  Developed for:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        11898 SW 128th Ave.  Tigard, Or  97223
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    04/2021:  2021.04    Initial revision
--
--
--  This file is part of OSVVM.
--  
--  Copyright (c) 2010 - 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  

library osvvm ; 
  context osvvm.OsvvmContext ; 
  
use std.textio.all ;

package NameStorePkg is

  type NameStoreIDType is record
    ID : integer ;
  end record NameStoreIDType ; 
  type NameStoreIDArrayType is array (integer range <>) of NameStoreIDType ;  

  impure function NewName   (NameIn : String) return NameStoreIDType ;
  procedure       Set       (ID : NameStoreIDType ; NameIn : String) ;
  impure function Get       (ID : NameStoreIDType ;  DefaultName : string := "") return string ;
  impure function GetOpt    (ID : NameStoreIDType) return string ;
  impure function IsSet     (ID : NameStoreIDType) return boolean ; 
  procedure       Clear     (ID : NameStoreIDType) ; -- clear name
  procedure       Deallocate(ID : NameStoreIDType) ; -- effectively alias to clear name

  type NameStorePType is protected  
    impure function NewName   (NameIn : String) return NameStoreIDType ;
    procedure       Set       (ID : NameStoreIDType ; NameIn : String) ;
    impure function Get       (ID : NameStoreIDType ;  DefaultName : string := "") return string ;
    impure function GetOpt    (ID : NameStoreIDType) return string ;
    impure function IsSet     (ID : NameStoreIDType) return boolean ; 
    procedure       Clear     (ID : NameStoreIDType) ; -- clear name
    procedure       Deallocate(ID : NameStoreIDType) ; -- effectively alias to clear name
  end protected NameStorePType ;

end package NameStorePkg ;

--- ///////////////////////////////////////////////////////////////////////////
--- ///////////////////////////////////////////////////////////////////////////
--- ///////////////////////////////////////////////////////////////////////////

package body NameStorePkg is
  type NameStorePType is protected body
  
    alias ItemType         is NamePType ; 
    type  ItemPtrType      is access ItemType ; 
    type  ItemArrayType    is array (integer range <>) of ItemPtrType ;  
    type  ItemArrayPtrType is access ItemArrayType ;
    
    variable NameArrayPtr   : ItemArrayPtrType ;
    variable NumItems       : integer := 0 ; 
--    constant MIN_NUM_ITEMS  : integer := 4 ; -- Temporarily small for testing
    constant MIN_NUM_ITEMS  : integer := 32 ; -- Min amount to resize array

    ------------------------------------------------------------
    -- Package Local
    function NormalizeArraySize( NewNumItems, MinNumItems : integer ) return integer is
    ------------------------------------------------------------
      variable NormNumItems : integer := NewNumItems ;
      variable ModNumItems  : integer := 0;
    begin
      ModNumItems := NewNumItems mod MinNumItems ; 
      if ModNumItems > 0 then 
        NormNumItems := NormNumItems + (MinNumItems - ModNumItems) ; 
      end if ; 
      return NormNumItems ; 
    end function NormalizeArraySize ;

    ------------------------------------------------------------
    -- Package Local
    procedure GrowNumberItems (
    ------------------------------------------------------------
      variable ItemArrayPtr     : InOut ItemArrayPtrType ;
      constant NewNumItems      : in integer ;
      constant CurNumItems      : in integer ;
      constant MinNumItems      : in integer 
    ) is
      variable oldItemArrayPtr  : ItemArrayPtrType ;
    begin
      if ItemArrayPtr = NULL then
        ItemArrayPtr := new ItemArrayType(1 to NormalizeArraySize(NewNumItems, MinNumItems)) ;
      elsif NewNumItems > ItemArrayPtr'length then
        oldItemArrayPtr := ItemArrayPtr ;
        ItemArrayPtr := new ItemArrayType(1 to NormalizeArraySize(NewNumItems, MinNumItems)) ;
        ItemArrayPtr.all(1 to CurNumItems) := oldItemArrayPtr.all(1 to CurNumItems) ;
        deallocate(oldItemArrayPtr) ;
      end if ;
      for i in CurNumItems + 1 to NewNumItems loop
        ItemArrayPtr(i) := new ItemType ;
      end loop ; 
    end procedure GrowNumberItems ;

    ------------------------------------------------------------
    impure function NewName (NameIn : String) return NameStoreIDType is
    ------------------------------------------------------------
      variable Result : NameStoreIDType ; 
      variable NewNumItems : integer ;
    begin
      NewNumItems := NumItems + 1 ; 
      GrowNumberItems(NameArrayPtr, NewNumItems, NumItems, MIN_NUM_ITEMS) ;
      Result.ID := NewNumItems ; 
      NumItems  := NewNumItems ;
      Set(Result, NameIn) ; 
      return Result ; 
    end function NewName ;

    ------------------------------------------------------------
    procedure Set (ID : NameStoreIDType ; NameIn : String) is
    ------------------------------------------------------------
    begin
      NameArrayPtr(ID.ID).set(NameIn) ;
    end procedure Set ;

    ------------------------------------------------------------
    impure function Get (ID : NameStoreIDType ; DefaultName : string := "") return string is
    ------------------------------------------------------------
    begin
      return NameArrayPtr(ID.ID).Get(DefaultName) ;
    end function Get ;

    ------------------------------------------------------------
    impure function GetOpt (ID : NameStoreIDType) return string is
    ------------------------------------------------------------
    begin
      return NameArrayPtr(ID.ID).Get ;
    end function GetOpt ;

    ------------------------------------------------------------
    impure function IsSet (ID : NameStoreIDType) return boolean is 
    ------------------------------------------------------------
    begin
      return NameArrayPtr(ID.ID).IsSet ;
    end function IsSet ;      
    
    ------------------------------------------------------------
    procedure Clear (ID : NameStoreIDType) is
    ------------------------------------------------------------
    begin
      NameArrayPtr(ID.ID).Clear ;
    end procedure Clear ;
    
    ------------------------------------------------------------
    procedure Deallocate(ID : NameStoreIDType) is
    ------------------------------------------------------------
    begin
      NameArrayPtr(ID.ID).Clear ;
    end procedure Deallocate ;
    
  end protected body NameStorePType ;


  variable NameStore : NameStorePType ; 
  
  ------------------------------------------------------------
  impure function NewName (NameIn : String) return NameStoreIDType is
  ------------------------------------------------------------
  begin
    return NameStore.NewName(NameIn) ;
  end function NewName ;

  ------------------------------------------------------------
  procedure Set (ID : NameStoreIDType ; NameIn : String) is
  ------------------------------------------------------------
  begin
    NameStore.set(ID, NameIn) ;
  end procedure Set ;

  ------------------------------------------------------------
  impure function Get (ID : NameStoreIDType ; DefaultName : string := "") return string is
  ------------------------------------------------------------
  begin
    return NameStore.Get(ID, DefaultName) ;
  end function Get ;

  ------------------------------------------------------------
  impure function GetOpt (ID : NameStoreIDType) return string is
  ------------------------------------------------------------
  begin
    return NameStore.Get(ID) ;
  end function GetOpt ;

  ------------------------------------------------------------
  impure function IsSet (ID : NameStoreIDType) return boolean is 
  ------------------------------------------------------------
  begin
    return NameStore.IsSet(ID) ;
  end function IsSet ;      
  
  ------------------------------------------------------------
  procedure Clear (ID : NameStoreIDType) is
  ------------------------------------------------------------
  begin
    NameStore.Clear(ID) ;
  end procedure Clear ;
  
  ------------------------------------------------------------
  procedure Deallocate(ID : NameStoreIDType) is
  ------------------------------------------------------------
  begin
    NameStore.Clear(ID) ;
  end procedure Deallocate ;


end package body NameStorePkg ;