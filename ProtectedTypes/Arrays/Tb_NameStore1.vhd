--
--  File Name:         Tb_NameStore1.vhd
--  Design Unit Name:  Tb_NameStore1
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis      jim@synthworks.com
--
--
--  Description:
--      VHDL-2019 Arrays of Protected Types
--
--
--  Developed by:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    04/2021   2021.04    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  
library ieee ;
  use ieee.std_logic_1164.all ;
  use ieee.numeric_std.all ;
  use ieee.numeric_std_unsigned.all ;
  use ieee.math_real.all ;
  
  use std.env.all ;

library osvvm ;
  context osvvm.OsvvmContext ;
    
  use work.NameStorePkg.all ; 
        
entity Tb_NameStore1 is
end entity Tb_NameStore1 ; 
architecture Test of Tb_NameStore1 is

  signal NameID1 : NameStoreIDType ; -- := NewName("String 1") ;
     
begin

  testProc : process
--    variable NameID2 : NameStoreIDType := NewName("String 2") ;
    variable NameID2 : NameStoreIDType ;
    variable NameIDArray : NameStoreIDArrayType(1 to 10) ;
  begin

    NameID1 <= NewName("String 1") ;

    wait for 1 ns ; 
    
    NameID2 := NewName("String 2") ;

    log("Test 1: Print Current Time") ; 
    print("Name 1:       " & Get(NameID1)) ; 
    print("Name 2:       " & Get(NameID2)) ; 
    
    wait for 1 ns ; 

    Set(NameID1, "New String 1") ;
    Set(NameID2, "New String 2") ;
    
    wait for 1 ns ; 

    print("Name 1:       " & Get(NameID1)) ; 
    print("Name 2:       " & Get(NameID2)) ; 
    
    for i in NameIDArray'range loop
      NameIDArray(i) := NewName("ArrayString " & To_String(i)) ;
    end loop ;
    
    print("Name 1:       " & Get(NameID1)) ; 
    print("Name 2:       " & Get(NameID2)) ; 
    for i in NameIDArray'range loop
      print("Array Name " & to_string(i) & ":       " & Get(NameIDArray(i))) ; 
    end loop ;

    for i in NameIDArray'range loop
      Set(NameIDArray(i), "New ArrayString " & To_String(i)) ;
    end loop ;

    for i in NameIDArray'range loop
      print("Array Name " & to_string(i) & ":       " & Get(NameIDArray(i))) ; 
    end loop ;

    wait ; 
  end process testProc ; 
  

end architecture Test ;