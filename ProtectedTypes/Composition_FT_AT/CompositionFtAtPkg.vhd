--
--  File Name:         CompositionFtAtPkg.vhd
--  Design Unit Name:  CompositionFtAtPkg
--  Revision:          STANDARD VERSION
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis          SynthWorks
--
--
--  Package Defines
--      Demonstration of VHDL-2019 features.  
--      Componsition with Protected Types, passing FT and AT to methods
--
--  Developed for:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        11898 SW 128th Ave.  Tigard, Or  97223
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    05/2021:  2021.05    Initial revision
--
--
--  This file is part of OSVVM.
--  
--  Copyright (c) 2010 - 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  

library osvvm ; 
  context osvvm.OsvvmContext ; 
  
use std.textio.all ;

package CompositionFtAtPkg is

  type CompositionFtAtPType is protected  
    private variable Name : NamePType ;
    alias SetName is Name.Set [string] ;
    alias GetName is Name.Get [string return string] ;

    procedure WriteNameToFile ( file f : text ; variable L : inout line ) ;
  end protected CompositionFtAtPType ;

end package CompositionFtAtPkg ;

--- ///////////////////////////////////////////////////////////////////////////
--- ///////////////////////////////////////////////////////////////////////////
--- ///////////////////////////////////////////////////////////////////////////

package body CompositionFtAtPkg is
  type CompositionFtAtPType is protected body
  
    procedure WriteNameToFile ( file f : text ; variable L : inout line ) is
      variable buf : line ; 
    begin
      Write(buf, GetName & ": ") ; 
      if L /= NULL then
        Write(buf, L.all) ; 
        deallocate (L) ; 
      end if ; 
      WriteLine(f, buf) ; 
    end procedure WriteNameToFile ; 

  end protected body CompositionFtAtPType ;

end package body CompositionFtAtPkg ;