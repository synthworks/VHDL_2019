--
--  File Name:         Tb_CompositionFtAt1.vhd
--  Design Unit Name:  Tb_CompositionFtAt1
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis      jim@synthworks.com
--
--
--  Description:
--      VHDL-2019 Arrays of Protected Types
--
--
--  Developed by:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    04/2021   2021.04    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  
library ieee ;
  use ieee.std_logic_1164.all ;
  use ieee.numeric_std.all ;
  use ieee.numeric_std_unsigned.all ;
  use ieee.math_real.all ;
  
  use std.env.all ;
  use std.textio.all ;

library osvvm ;
  context osvvm.OsvvmContext ;
    
  use work.CompositionFtAtPkg.all ; 
        
entity Tb_CompositionFtAt1 is
end entity Tb_CompositionFtAt1 ; 
architecture Test of Tb_CompositionFtAt1 is

  shared variable Name1 : CompositionFtAtPType ; 
  
  file File1 : text open write_mode is "CompositionFtAt1.txt" ;
     
begin

  testProc : process
    variable Name2 : CompositionFtAtPType ; 
    variable buf, buf2 : line := NULL ; 
  begin

    Name1.SetName("Name 1") ;
    Name2.SetName("Name 2") ;
    
    Name1.WriteNameToFile(OUTPUT, buf) ; 
    Name2.WriteNameToFile(OUTPUT, buf) ; 
    
    swrite(buf, "Fred Flintstone") ;
    Name1.WriteNameToFile(OUTPUT, buf) ; 
    swrite(buf, "Barney Rubble") ;
    Name2.WriteNameToFile(OUTPUT, buf) ; 
    
    swrite(buf, "Fred Flintstone") ;
    Name1.WriteNameToFile(File1, buf) ; 
    swrite(buf, "Barney Rubble") ;
    Name2.WriteNameToFile(File1, buf) ; 

    swrite(buf2, LF & "Verify after printing, buf is NULL") ;
    WriteLine(File1, buf2) ; 
    Name1.WriteNameToFile(File1, buf) ; 
    Name2.WriteNameToFile(File1, buf) ; 

    file_close(File1) ; 

    wait ; 
  end process testProc ; 
  

end architecture Test ;