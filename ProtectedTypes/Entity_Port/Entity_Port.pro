library osvvm_TbAxi4_SharedMemoryVti
analyze TestCtrl_e.vhd
analyze TbAxi4Memory.vhd

# Run as a basic sanity test - only runs AxiBus1
analyze TbAxi4_MemoryBurst1.vhd
simulate TbAxi4_MemoryBurst1