--
--  File Name:         TestCtrl_e.vhd
--  Design Unit Name:  TestCtrl
--  Revision:          OSVVM MODELS STANDARD VERSION
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis      jim@synthworks.com
--
--
--  Description:
--      Test transaction source
--
--
--  Developed by:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    05/2021   2021.05    Initial revision
--
--
--  This file is part of OSVVM.
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  

library ieee ;
  use ieee.std_logic_1164.all ;
  use ieee.numeric_std.all ;
  use ieee.numeric_std_unsigned.all ;
  use ieee.math_real.all ;
  
library OSVVM ; 
  context OSVVM.OsvvmContext ; 

library OSVVM_AXI4 ;
  context OSVVM_AXI4.Axi4Context ; 

entity TestCtrl is
  port (
    -- Global Signal Interface
    nReset         : In    std_logic
  ) ;
  
  -- Connect transaction interfaces using external names
  -- MasterRec TransRec are in separate components
  -- Extra set of external names to device 1 Master and Memory Responder_1
  -- Allows any existing test to run
  alias MasterRec is <<signal ^.Master_1.TransRec : AddressBusRecType>> ;
  alias WriteBurstFifo is <<variable ^.Master_1.WriteBurstFifo : osvvm.ScoreboardPkg_slv.ScoreboardPType>> ;
  alias ReadBurstFifo  is <<variable ^.Master_1.ReadBurstFifo  : osvvm.ScoreboardPkg_slv.ScoreboardPType>> ;
  alias ResponderRec is <<signal ^.ResponderRec : AddressBusRecType>> ;
  
--  alias MasterRec1 is <<signal ^.Master_1.TransRec : AddressBusRecType>> ;
--  alias WriteBurstFifo1 is <<variable ^.Master_1.WriteBurstFifo : osvvm.ScoreboardPkg_slv.ScoreboardPType>> ;
--  alias ReadBurstFifo1  is <<variable ^.Master_1.ReadBurstFifo  : osvvm.ScoreboardPkg_slv.ScoreboardPType>> ;
--  alias ResponderRec1 is <<signal ^.Responder_1.TransRec1 : AddressBusRecType>> ;

--  alias MasterRec2 is <<signal ^.Master_2.TransRec : AddressBusRecType>> ;
--  alias WriteBurstFifo2 is <<variable ^.Master_2.WriteBurstFifo : osvvm.ScoreboardPkg_slv.ScoreboardPType>> ;
--  alias ReadBurstFifo2  is <<variable ^.Master_2.ReadBurstFifo  : osvvm.ScoreboardPkg_slv.ScoreboardPType>> ;
--  alias ResponderRec2 is <<signal ^.Responder_1.TransRec2 : AddressBusRecType>> ;
--
--  alias MasterRec3 is <<signal ^.Master_3.TransRec : AddressBusRecType>> ;
--  alias WriteBurstFifo3 is <<variable ^.Master_3.WriteBurstFifo : osvvm.ScoreboardPkg_slv.ScoreboardPType>> ;
--  alias ReadBurstFifo3  is <<variable ^.Master_3.ReadBurstFifo  : osvvm.ScoreboardPkg_slv.ScoreboardPType>> ;
--  alias ResponderRec3 is <<signal ^.Responder_1.TransRec3 : AddressBusRecType>> ;
--
--  alias MasterRec4 is <<signal ^.Master_4.TransRec : AddressBusRecType>> ;
--  alias WriteBurstFifo4 is <<variable ^.Master_4.WriteBurstFifo : osvvm.ScoreboardPkg_slv.ScoreboardPType>> ;
--  alias ReadBurstFifo4  is <<variable ^.Master_4.ReadBurstFifo  : osvvm.ScoreboardPkg_slv.ScoreboardPType>> ;
--  alias ResponderRec4 is <<signal ^.Responder_1.TransRec4 : AddressBusRecType>> ;

  
  -- Derive useful AXI interface properties from the MasterRec/MasterRec1
  constant AXI_ADDR_WIDTH : integer := MasterRec.Address'length ; 
  constant AXI_DATA_WIDTH : integer := MasterRec.DataToModel'length ;  
  constant AXI_DATA_BYTE_WIDTH : integer := AXI_DATA_WIDTH / 8 ;
  constant AXI_BYTE_ADDR_WIDTH : integer := integer(ceil(log2(real(AXI_DATA_BYTE_WIDTH)))) ;

end entity TestCtrl ;
