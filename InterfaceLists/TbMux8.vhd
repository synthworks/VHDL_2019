--
--  File Name:         TbMux8.vhd
--  Design Unit Name:  TbMux8
--  Revision:          STANDARD VERSION
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis          SynthWorks
--
--
--  Defines  TbMux8 testbench for Mux8
--
--  Developed for:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        11898 SW 128th Ave.  Tigard, Or  97223
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    05/2021:  2021.05    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  

library ieee ; 
  use ieee.std_logic_1164.all ; 
  use ieee.numeric_std_unsigned.all ; 
  
  use work.ConversionPkg.all ;
  
library osvvm ; 
  context osvvm.OsvvmContext ; 
  
entity TbMux8 is 
end entity TbMux8 ; 
Architecture Test of TbMux8 is
  component Mux8 is 
    port (
      Sel        : in  std_logic_vector (2 downto 0) ; 
      A          : in  std_logic_vector ;  
      B, C, D    : in  A'subtype ;                       -- Interface Lists are Ordered
      E, F, G, H : in  A'subtype ;
      Y          : out A'subtype ;   
    ) ;
  end ; -- Regularize Component Declarations - no comopnent or Mux8 required
  
  signal Sel        : std_logic_vector (2 downto 0) ; 
  signal A          : std_logic_vector := to_slv(0, 8) ;  -- Inferring Signal Constraints from Init
  signal B          : A'subtype := to_slv(1) ;            -- Function Knows Output Subtype
  signal C          : A'subtype := to_slv(2) ;
  signal D          : A'subtype := to_slv(3) ;
  signal E          : A'subtype := to_slv(4) ;
  signal F          : A'subtype := to_slv(5) ;
  signal G          : std_logic_vector := to_slv(6, 8) ;  -- Inferring Signal Constraints from Init
  signal H          : std_logic_vector := to_slv(7, 8) ;  -- Inferring Signal Constraints from Init
  signal Y          : A'subtype ; 
  
begin

  Mux8_1 : Mux8  
    port map ( Sel, A, B, C, D, E, F, G, H, Y ) ;


  TestProc : process
  begin
    SetLogEnable(PASSED, TRUE) ; 
    for i in 0 to 255 loop 
      Sel <= to_slv(i mod 8) ;
      wait for 5 ns ; 
      AffirmIfEqual(Y, to_slv(i mod 8, 8), "test case " & to_string(i) & " ") ; 
      wait for 5 ns ; 
    end loop ;
    
    ReportAlerts ; 
    std.env.stop(GetAlertCount) ; 
    wait ; 
  end process TestProc ; 
  
end architecture Test ; 