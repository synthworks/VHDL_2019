--
--  File Name:         MuxPkg.vhd
--  Design Unit Name:  MuxPkg
--  Revision:          STANDARD VERSION
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis          SynthWorks
--
--
--  Package Defines
--      Simple Mux4 function and procedures
--
--  Developed for:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        11898 SW 128th Ave.  Tigard, Or  97223
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    05/2021:  2021.05    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  

library ieee ; 
  use ieee.std_logic_1164.all ; 
  
package MuxPkg is

  function Mux4 (
    Sel        : std_logic_vector (1 downto 0) ; 
    A          : std_logic_vector ;  
    B, C, D    : A'Subtype ;                          -- Interface Lists are Ordered
  ) return std_logic_vector ; 

  procedure Mux4 (
    constant Sel        : in  std_logic_vector (1 downto 0) ; 
    constant A          : in  std_logic_vector ;  
    constant B, C, D    : in  A'Subtype ;             -- Interface Lists are Ordered
    signal   Y          : out A'Subtype ; 
  ) ;


end package MuxPkg ;

--- ///////////////////////////////////////////////////////////////////////////
--- ///////////////////////////////////////////////////////////////////////////
--- ///////////////////////////////////////////////////////////////////////////

package body MuxPkg is
  
  function Mux4 (
    Sel        : std_logic_vector (1 downto 0) ; 
    A          : std_logic_vector ;  
    B, C, D    : A'Subtype ;
  ) return std_logic_vector is
  begin
    return A when Sel = "00" else B when Sel = "01" ;
    return C when Sel = "10" ;
    return D when Sel = "11" ; 
    return (A'range => 'X') ;     
  end function Mux4 ; 

  procedure Mux4 (
    constant Sel        : in  std_logic_vector (1 downto 0) ; 
    constant A          : in  std_logic_vector ;  
    constant B, C, D    : in  A'Subtype ;
    signal   Y          : out A'Subtype ; 
  ) is
  begin
    Y <= A ; 
    return when Sel = "00" ;
    Y <= B ; 
    return when Sel = "01" ;
    Y <= C ; 
    return when Sel = "10" ;
    Y <= D ; 
    return when Sel = "11" ;
    Y <= (A'range => 'X') ; 
  end procedure Mux4 ; 

end package body MuxPkg ;