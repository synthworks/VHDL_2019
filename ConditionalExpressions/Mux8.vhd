--
--  File Name:         Mux8.vhd
--  Design Unit Name:  Mux8
--  Revision:          STANDARD VERSION
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis          SynthWorks
--
--
--  Defines  Mux8 design by calling Mux4 function and procedure 
--
--  Developed for:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        11898 SW 128th Ave.  Tigard, Or  97223
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    05/2021:  2021.05    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  

library ieee ; 
  use ieee.std_logic_1164.all ; 
  
  use work.MuxPkg.all ; 
  
entity Mux8 is 
  port (
    Sel        : in  std_logic_vector (2 downto 0) ; 
    A          : in  std_logic_vector ;  
    B, C, D    : in  A'subtype ;  -- Interface Lists are Ordered
    E, F, G, H : in  A'subtype ;
    Y          : out A'subtype ;
  ) ;
end entity Mux8 ; 
Architecture RTL of Mux8 is

  signal Y1, Y2 : A'Subtype ;
  signal Clk : std_logic := '0' ; 

begin
  Clk <= not Clk after (1 ns when NOW < 1 us else 2 ns) ; 
  
  Y1 <= Mux4(Sel(1 downto 0), A, B, C, D) ; 
  Mux4(Sel(1 downto 0), E, F, G, H, Y2) ; 
  
  Y <= (Y1 when not Sel(2) else Y2) when rising_edge(Clk)  ;
  
end architecture RTL ; 