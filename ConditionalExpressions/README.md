# Interface Lists
This example includes the following examples:

## Optional `;`
Entity Mux 8  using a MUX4 function and MUX4 Procedure

## Regularized component
Ending minimized to:  `end ;`

## Interface List Ordered

## Function Knows Output Subtype

## Sizing Signal from Init