--
--  File Name:         Tb_FileState1.vhd
--  Design Unit Name:  Tb_FileState1
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis      jim@synthworks.com
--
--
--  Description:
--      VHDL-2019 FileIO FILE_STATE
--
--
--  Developed by:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    04/2021   2021.04    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  
library ieee ;
  use ieee.std_logic_1164.all ;
  use ieee.numeric_std.all ;
  use ieee.numeric_std_unsigned.all ;
  use std.textio.all ;

library osvvm ;
    context osvvm.OsvvmContext ;
        
entity Tb_FileState1 is
end entity Tb_FileState1 ; 
architecture Test of Tb_FileState1 is
  file TranscriptFile : text ; 
begin

  testProc : process
    variable buf : line ; 
  begin
  
    for i in 1 to 2 loop 
      log("Test " & to_string(i) & " : FILE_STATE ") ; 
      
      -- STATE_CLOSED on 1st itereration because file opened after
      if file_state(TranscriptFile) = STATE_CLOSED then
        write(buf, "FILE_STATE = " & to_string(file_state(TranscriptFile)) );
        WriteLine(OUTPUT, buf) ; 
      else
        write(buf, "FILE_STATE = " & to_string(file_state(TranscriptFile)) );
        WriteLine(TranscriptFile, buf) ; 
        
        print("FILE_STATE = " & to_string(file_state(TranscriptFile)) );
      end if ; 
      
      -- Will FAIL on 2nd itereration because file already open
      if FILE_OPEN (TranscriptFile, "FileState1.txt", WRITE_MODE) = OPEN_OK then
        print("File_Open = OPEN_OK") ;
      else
        print("File_Open FAILED") ; 
      end if ; 
    end loop ;

    File_Close(TranscriptFile) ; 
    wait ; 
  end process testProc ; 
  

end architecture Test ;