--
--  File Name:         Tb_ReadWriteMode1.vhd
--  Design Unit Name:  Tb_ReadWriteMode1
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis      jim@synthworks.com
--
--
--  Description:
--      VHDL-2019 FileIO READ_WRITE_MODE
--
--
--  Developed by:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    04/2021   2021.04    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  
library ieee ;
  use ieee.std_logic_1164.all ;
  use ieee.numeric_std.all ;
  use ieee.numeric_std_unsigned.all ;

library osvvm ;
    context osvvm.OsvvmContext ;
        
entity Tb_ReadWriteMode1 is
end entity Tb_ReadWriteMode1 ; 
architecture Test of Tb_ReadWriteMode1 is
  type BinFileType is file of character ;
  file BinFile : BinFileType open READ_WRITE_MODE is "ReadWriteMode1.bin" ;
  constant POS_A : integer := character'pos('A') ; 
  constant POS_0 : integer := character'pos('0') ; 
  
begin

  testProc : process
    variable char : character ; 
  begin
    log("Test1: Write then Read") ; 
    for i in 1 to 10 loop
      write(BinFile, character'val(POS_A + i - 1)) ;
    end loop ;
    
    file_rewind(BinFile) ;
    
    for i in 1 to 10 loop
      read(BinFile, char) ;
      AffirmIfEqual(char, character'val(POS_A + i - 1)) ;
      print(""&char) ;
    end loop ;
    
    file_rewind(BinFile) ;
    
    log("Test2: Modify File") ; 
    for i in 1 to 10 loop
      if i mod 2 = 0 then 
        write(BinFile, character'val(POS_0 + i - 1)) ;
      else
        read(BinFile, char) ; 
      end if ; 
    end loop ;

    file_rewind(BinFile) ;
    
    for i in 1 to 10 loop
      read(BinFile, char) ;
      print(""&char) ;
    end loop ;

    File_Close(BinFile) ; 
    wait ; 
  end process testProc ; 
  

end architecture Test ;