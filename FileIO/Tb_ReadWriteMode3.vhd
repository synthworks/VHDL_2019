--
--  File Name:         Tb_ReadWriteMode3.vhd
--  Design Unit Name:  Tb_ReadWriteMode3
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis      jim@synthworks.com
--
--
--  Description:
--      VHDL-2019 FileIO READ_WRITE_MODE
--
--
--  Developed by:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    04/2021   2021.04    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  
library ieee ;
  use ieee.std_logic_1164.all ;
  use ieee.numeric_std.all ;
  use ieee.numeric_std_unsigned.all ;
  use std.textio.all ;

library osvvm ;
    context osvvm.OsvvmContext ;
        
entity Tb_ReadWriteMode3 is
end entity Tb_ReadWriteMode3 ; 
architecture Test of Tb_ReadWriteMode3 is
  file MyFile : text open READ_WRITE_MODE is "ReadWriteMode3.txt" ; 
  constant POS_A : integer := character'pos('A') ; 
  constant POS_0 : integer := character'pos('0') ; 
begin

  testProc : process
    variable word : string(1 to 4) ; 
    variable char : character ; 
    variable wbuf, rbuf : line ; 
  begin
    log("Test1: Write then Read") ; 
    for i in 0 to 5 loop
      for j in 0 to 3 loop
        write(wbuf, character'val(POS_A + j + i*4)) ;
      end loop ;
      writeline(MyFile, wbuf) ; 
    end loop ;
    
    file_rewind(MyFile) ;
    
    for i in 0 to 5 loop
      readline(MyFile, rbuf) ;
      for j in 1 to 4 loop 
        read(rbuf, char) ; 
        write(wbuf, char) ;
      end loop ;
      writeline(OUTPUT, wbuf) ; 
    end loop ;
    
    file_rewind(MyFile) ;
    
    log("Test2: Modify File") ;
    for i in 0 to 5 loop
      if i mod 2 = 0 then 
        for j in 0 to 3 loop 
          write(wbuf, character'val(POS_0 + j + 4*(i/2))) ;
          -- print("" & character'val(POS_0 + j + 4*(i/2)) ) ; 
        end loop ;
        writeline(MyFile, wbuf) ; 
      else
        readline(MyFile, rbuf) ;
        -- Just skipping over the line
      end if ; 
    end loop ;
  
    file_rewind(MyFile) ;
    
    for i in 0 to 5 loop
      readline(MyFile, rbuf) ;
      read(rbuf, word) ; 
      print(word) ;
    end loop ;

    File_Close(MyFile) ; 
    wait ; 
  end process testProc ; 
  

end architecture Test ;