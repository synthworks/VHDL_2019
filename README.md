# VHDL 2019
__TOC__

## Examples for Part 1:  

## Examples for Part 2:

## Examples from Part 3:  RTL Enhancements   

### Conditional expressions   
See ConditionalExpressions: Mux8.vhd, MuxPkg.vhd

### Conditional return   
See ConditionalExpressions/MuxPkg.vhd

### Inferring signal and variable constraints from initial values   
See InterfaceLists/TbMux8.vhd   

### All interface lists are ordered   
See InterfaceLists: TbMux8.vhd, Mux8.vhd, MuxPkg.vhd   

### Allow functions to know the output vector size
See ConversionPkg/ConversionPkg.vhd (to_slv) and TbConversionPkg.vhd
TbConversionPkg_InjectedErrors.vhd
TbConversionPkg_ufixed.vhd (to_ufixed)

### Optional semicolon at the end of interface list
See InterfaceLists: TbMux8.vhd, Mux8.vhd, MuxPkg.vhd   

### Component declaration syntax regularization 
See InterfaceLists/TbMux8.vhd 

### Empty Records 
See EmptyRecord/TbEmptyRecord.vhd

### Attributes of Enumerated Types
None

### Range Expressions
See RangeExpressions/TbRangeExpressions.vhd


