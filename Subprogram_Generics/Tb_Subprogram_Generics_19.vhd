--
--  File Name:         Tb_Subprogram_Generics_19.vhd
--  Design Unit Name:  Tb_Subprogram_Generics_19
--  Revision:          STANDARD VERSION
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis       SynthWorks
--
--
--  Tests:  Function with Generic Type using 2019
--
--  Developed for:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        11898 SW 128th Ave.  Tigard, Or  97223
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    06/2021:  2021.06    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  

library ieee ; 
  use ieee.std_logic_1164.all ; 
  use ieee.numeric_std.all ; 
  use ieee.numeric_std_unsigned.all ; 
  use ieee.math_real.all ; 
  
library osvvm ; 
  context osvvm.OsvvmContext ; 
  use osvvm.RandomBasePkg.all ; 
  
  use work.GenericMuxPkg.all ;
  
entity Tb_Subprogram_Generics_19 is 
end entity Tb_Subprogram_Generics_19 ; 
Architecture Test of Tb_Subprogram_Generics_19 is
 
  signal Sel           : std_logic_vector(1 downto 0) ; 
  signal A, B, C, D, Y : std_logic_vector(7 downto 0) ; 
  
  -- function Mux4 is new GenericMux4 generic map (ParamType => A'subtype) ; 
  

begin

--  Y <= GenericMux4 generic map (ParamType => A'subtype)(Sel, A, B, C, D) ; 
--  Y <= GenericMux4 generic map (ParamType => std_logic_vector)(Sel, A, B, C, D) ; 
  Y <= GenericMux4 generic map (A'subtype)(Sel, A, B, C, D) ; 

  TestProc : process
  begin
    SetLogEnable(PASSED, TRUE) ; 
    SetAlertLogName("Tb_Subprogram_Generics_19") ;
    
    A <= X"11"; B <= X"22"; C <= X"33"; D <= X"44";
    
    for i in 0 to 15 loop
      Sel <= to_slv(i mod 4, 2) ; 
      wait for 5 ns ;  
      case i mod 4 is
        when 0 =>  AffirmIfEqual(Y, A, "i = " & to_string(i) & " ") ; 
        when 1 =>  AffirmIfEqual(Y, B, "i = " & to_string(i) & " ") ;  
        when 2 =>  AffirmIfEqual(Y, C, "i = " & to_string(i) & " ") ;  
        when 3 =>  AffirmIfEqual(Y, D, "i = " & to_string(i) & " ") ;  
        when others => Alert("Should not execute this") ; 
      end case ; 
      wait for 5 ns ;       
    end loop ;

    ReportAlerts ; 
    std.env.stop(0) ; 
    wait ; 
  end process TestProc ; 
  
end architecture Test ; 