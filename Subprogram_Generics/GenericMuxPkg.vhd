--
--  File Name:         GenericMuxPkg.vhd
--  Design Unit Name:  GenericMuxPkg
--  Revision:          STANDARD VERSION
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis          SynthWorks
--
--
--  Package Defines
--      Mux4 with A being a generic type
--
--  Developed for:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        11898 SW 128th Ave.  Tigard, Or  97223
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    06/2021:  2021.06    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  

library ieee ; 
  use ieee.std_logic_1164.all ;
  use ieee.numeric_std_unsigned.all ;  
  use ieee.fixed_pkg.all ; 
  
  use std.textio.all ;

package GenericMuxPkg is

  function GenericMux4 
  Generic ( type ParamType ) 
  Parameter( 
    Sel        : std_logic_vector (1 downto 0) ; 
    A          : ParamType ;
    B, C, D    : A'subtype
  ) return A'subtype ;
  
end package GenericMuxPkg ;

--- ///////////////////////////////////////////////////////////////////////////
--- ///////////////////////////////////////////////////////////////////////////
--- ///////////////////////////////////////////////////////////////////////////

package body GenericMuxPkg is

  function GenericMux4 
  Generic ( type ParamType ) 
  Parameter( 
    Sel        : std_logic_vector (1 downto 0) ; 
    A          : ParamType ;
    B, C, D    : A'subtype
  ) return A'subtype is
    variable X : A'subtype ; 
  begin
    case Sel is 
      when "00" =>     X := A ;
      when "01" =>     X := B ;
      when "10" =>     X := C ;
      when "11" =>     X := D ;
      when others =>   null ; -- X := ParamType'left
    end case ; 
    return X ; 
  end function GenericMux4 ;

end package body GenericMuxPkg ;