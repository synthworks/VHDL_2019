--
--  File Name:         TbRangeExpressions.vhd
--  Design Unit Name:  TbRangeExpressions
--  Revision:          STANDARD VERSION
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis          SynthWorks
--
--
--  Defines  TbRangeExpressions 
--
--  Developed for:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        11898 SW 128th Ave.  Tigard, Or  97223
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    05/2021:  2021.05    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  

library ieee ; 
  use ieee.std_logic_1164.all ; 
  use ieee.numeric_std_unsigned.all ; 
  use ieee.fixed_pkg.all ; 
    
library osvvm ; 
  context osvvm.OsvvmContext ; 
  
entity TbRangeExpressions is 
end entity TbRangeExpressions ; 
Architecture Test of TbRangeExpressions is

  signal A4 : ufixed(3 downto -3) ; 
  
--  alias IntegerRangeRecType is integer'range'record ; 
  subtype IntegerRangeRecType is integer'range'record ; 
  
  constant B4_RANGE : IntegerRangeRecType := (
    Left      =>  3, 
    Right     => -3,
    Direction => DESCENDING
  ) ; 
  signal   B4       : ufixed(B4_RANGE) ;   
  
  constant C4_RANGE : IntegerRangeRecType := A4'range'value ; 
--  signal   C4       : ufixed(C4_RANGE) ; 
  signal   C4       : ufixed(B4_RANGE) ; 
  
-- Probably not allowed due to standard wording - see LRM issue #196  
--  constant Y5_RANGE : IntegerRangeRecType := (4 downto -3)'value ;
  constant Y5_RANGE : IntegerRangeRecType := (
    Left      =>  4, 
    Right     => -3,
   Direction => DESCENDING
  ) ; 
  signal   Y5, Z5   : ufixed(Y5_RANGE) ;

begin


  TestProc : process
  begin
    A4 <= to_ufixed(3.5, 3, -3) ; 
    B4 <= to_ufixed(1.5, B4) ; 
    C4 <= to_ufixed(1.1, C4) ; 
--    C4 <= to_ufixed(1.1, 3, -3) ; 
    
    wait for 0 ns ; 
    print("A4 = " & to_string(A4)) ; 
    print("B4 = " & to_string(B4)) ; 
    print("C4 = " & to_string(C4)) ; 
    
    wait for 5 ns ; 
    Y5 <= A4 + B4 ; 
    Z5 <= A4 + C4 ;
    
    wait for 0 ns ; 
    print("Y5 = " & to_string(Y5)) ; 
    print("Z5 = " & to_string(Z5)) ; 
    
    std.env.stop(0) ; 
    wait ; 
  end process TestProc ; 
  
end architecture Test ; 