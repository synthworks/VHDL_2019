--
--  File Name:         Tb_DateTime1.vhd
--  Design Unit Name:  Tb_DateTime1
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis      jim@synthworks.com
--
--
--  Description:
--      VHDL-2019 Date and Time
--
--
--  Developed by:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    04/2021   2021.04    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  
library ieee ;
  use ieee.std_logic_1164.all ;
  use ieee.numeric_std.all ;
  use ieee.numeric_std_unsigned.all ;
  use ieee.math_real.all ;
  
  use std.env.all ;

library osvvm ;
    context osvvm.OsvvmContext ;
        
entity Tb_DateTime1 is
end entity Tb_DateTime1 ; 
architecture Test of Tb_DateTime1 is
  constant BUILD_REC: TIME_RECORD := GMTIME ; 
  constant HW_BUILD_TIME : std_logic_vector :=
     to_slv(BUILD_REC.hour, 5) & 
     to_slv(BUILD_REC.minute, 6) & 
     to_slv(BUILD_REC.day, 5) & 
     to_slv(BUILD_REC.month, 4) & 
     to_slv(BUILD_REC.year rem 100, 7) ;  
     
begin

  testProc : process
    variable StartTime   : Real;  -- time in seconds 
    variable ElapsedTime : integer;
  begin
    StartTime := epoch ; 

    log("Test 1: Print Current Time") ; 
    print("Local Time:       " & to_string(LOCALTIME)) ; 
    print("Local Time+:      " & to_string(LOCALTIME, 3)) ; 
    print("Local Epoch Time: " & to_string(LOCALTIME(StartTime))) ; 
    print("Local GM Time:    " & to_string(LOCALTIME(GMTIME))) ; 
    print("GM Time:          " & to_string(GMTIME)) ; 
    print("GM Epoch Time:    " & to_string(GMTIME(StartTime))) ; 
    print("GM Local Time:    " & to_string(GMTIME(LOCALTIME))) ; 


    log("Test 2: HW_BUILD_TIME") ; 
    print("year:    " & to_string(to_integer(HW_BUILD_TIME(20 to 26))) ) ; 
    print("month:   " & to_string(1 + to_integer(HW_BUILD_TIME(16 to 19))) ) ; 
    print("day:     " & to_string(to_integer(HW_BUILD_TIME(11 to 15))) ) ; 
    print("hour:    " & to_string(to_integer(HW_BUILD_TIME( 0 to  4))) ) ; 
    print("minutes: " & to_string(to_integer(HW_BUILD_TIME( 5 to 10))) ) ; 
    
    
    log("Test 3: Measure Elapsed Clock CPU Time") ; 

    ElapsedTime := integer( round ( epoch - StartTime ) ) ;
   
    Print("Test took " & to_string(ElapsedTime) & " seconds") ;

    wait ; 
  end process testProc ; 
  

end architecture Test ;