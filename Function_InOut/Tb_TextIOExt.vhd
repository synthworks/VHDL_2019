--
--  File Name:         Tb_TextIOExt.vhd
--  Design Unit Name:  Tb_TextIOExt
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis      jim@synthworks.com
--
--
--  Description:
--      Test TextIOExtPkg
--
--
--  Developed by:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    06/2021   2021.06    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  
library ieee ;
  use ieee.std_logic_1164.all ;
  use ieee.numeric_std.all ;
  use ieee.numeric_std_unsigned.all ;
  use std.textio.all ;

library osvvm ;
    context osvvm.OsvvmContext ;
    
use work.TextIOExtPkg.all ; 
        
entity Tb_TextIOExt is
end entity Tb_TextIOExt ; 
architecture Test of Tb_TextIOExt is
  file ReadFile : text open READ_MODE is "../Function_InOut/TextIOExt.txt" ; 

begin

  testProc : process
    variable wbuf, rbuf : line ; 
    variable Char : character ;
  begin
    SetLogEnable(PASSED, TRUE) ; 
    
    -- read first line
    readline(ReadFile, rbuf) ;
    print(to_string(rbuf, FALSE)) ; 
    AffirmIfEqual(EndLine(rbuf), FALSE, ("EndLine at beginning of line " )) ;
    
    SkipSpaceTab(rbuf) ; 
    
    -- skip text
    loop
      exit when rbuf = NULL or rbuf.all'length = 0 ; 
      exit when rbuf(rbuf'left) = ' ' or rbuf(rbuf'left) = HT ; 
      read(rbuf, Char) ; 
    end loop ;
    
    AffirmIfEqual(EndLine(rbuf), FALSE, ("EndLine at middle of line " )) ;
    SkipSpaceTab(rbuf) ; 

    -- skip text
    loop
      exit when rbuf = NULL or rbuf.all'length = 0 ; 
      exit when rbuf(rbuf'left) = ' ' or rbuf(rbuf'left) = HT ; 
      read(rbuf, Char) ; 
    end loop ;
    
    AffirmIfEqual(EndLine(rbuf), FALSE, ("EndLine at end of line, but has space chars " )) ;
    SkipSpaceTab(rbuf) ; 
    AffirmIfEqual(EndLine(rbuf), TRUE, ("EndLine at end of line after removing space chars ")) ;

    
    -- read second line
    readline(ReadFile, rbuf) ;
    print(to_string(rbuf, FALSE)) ; 
    AffirmIfEqual(EmptyLine(rbuf), FALSE, ("EmptyLine at beginning of line " )) ;
    
    SkipSpaceTab(rbuf) ; 
    
    -- skip text
    loop
      exit when rbuf = NULL or rbuf.all'length = 0 ; 
      exit when rbuf(rbuf'left) = ' ' or rbuf(rbuf'left) = HT ; 
      read(rbuf, Char) ; 
    end loop ;
    
    AffirmIfEqual(EmptyLine(rbuf), FALSE, ("EmptyLine at middle of line " )) ;
    SkipSpaceTab(rbuf) ; 

    -- skip text
    loop
      exit when rbuf = NULL or rbuf.all'length = 0 ; 
      exit when rbuf(rbuf'left) = ' ' or rbuf(rbuf'left) = HT ; 
      read(rbuf, Char) ; 
    end loop ;
    
    AffirmIfEqual(EmptyLine(rbuf), TRUE, ("EmptyLine at end of line, but has space chars " )) ;
    SkipSpaceTab(rbuf) ; 
    AffirmIfEqual(EmptyLine(rbuf), TRUE, ("EmptyLine at end of line after removing space chars ")) ;


    File_Close(ReadFile) ; 
    wait ; 
  end process testProc ; 
  

end architecture Test ;