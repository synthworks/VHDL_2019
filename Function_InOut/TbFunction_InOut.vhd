--
--  File Name:         TbFunction_InOut.vhd
--  Design Unit Name:  TbFunction_InOut
--  Revision:          STANDARD VERSION
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis       SynthWorks
--
--
--  Tests:  Functions with InOut of an ordinary type
--
--  Developed for:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        11898 SW 128th Ave.  Tigard, Or  97223
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    06/2021:  2021.06    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  

library ieee ; 
  use ieee.std_logic_1164.all ; 
  use ieee.numeric_std.all ; 
  use ieee.numeric_std_unsigned.all ; 
  use ieee.math_real.all ; 
  
library osvvm ; 
  context osvvm.OsvvmContext ; 
  use osvvm.RandomBasePkg.all ; 
  
entity TbFunction_InOut is 
end entity TbFunction_InOut ; 
Architecture Test of TbFunction_InOut is

  impure function RandInt(
    Seed : inout RandomSeedType ;  
    Min  : in    integer ;
    Max  : in    integer
  ) return integer is
    variable rRandomVal : real ;
  begin
    Uniform (
      Seed(Seed'left), Seed(Seed'right), rRandomVal) ;
    return integer(round(rRandomVal*real(Max - Min + 1) - 0.5)) + Min ;
  end function RandInt ; 

begin

  TestProc : process
    variable Seed : RandomSeedType := GenRandSeed("TestProc") ; 
    variable RV   : RandomPType ; 
    variable int1 : integer ; 
    variable int2 : integer ; 
  begin
    SetLogEnable(PASSED, TRUE) ; 
    
    RV.InitSeed("TestProc") ;
    
    for i in 1 to 10 loop
      int1 := RandInt(Seed, 1, 100) ; 
      int2 := RV.RandInt(1, 100) ; 
      AffirmIfEqual(int1, int2, "Index " & to_string(i) & "  "); 
    end loop ;

    std.env.stop(0) ; 
    wait ; 
  end process TestProc ; 
  
end architecture Test ; 