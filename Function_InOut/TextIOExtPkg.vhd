--
--  File Name:         TextIOExtPkg.vhd
--  Design Unit Name:  TextIOExtPkg
--  Revision:          STANDARD VERSION
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis          SynthWorks
--
--
--  Package Defines
--      Extensions to TextIO package
--        EndLine, EmptyLine, 
--        To_String [line, boolean return string]
--
--  Developed for:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        11898 SW 128th Ave.  Tigard, Or  97223
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    06/2021:  2021.06    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  

library ieee ; 
  use ieee.std_logic_1164.all ;
  use ieee.numeric_std_unsigned.all ;  
  use ieee.fixed_pkg.all ; 
  
  use std.textio.all ;

package TextIOExtPkg is

  procedure SkipSpaceTab ( variable L : inout line) ;

  impure function EndLine(
      variable L     : inout Line 
  ) return boolean ;

  impure function EmptyLine (
      variable L     : inout Line 
  ) return boolean ;

  impure function to_string (variable L : inout line; Erase : boolean := TRUE) return string ; 

end package TextIOExtPkg ;

--- ///////////////////////////////////////////////////////////////////////////
--- ///////////////////////////////////////////////////////////////////////////
--- ///////////////////////////////////////////////////////////////////////////

package body TextIOExtPkg is

  procedure SkipSpaceTab ( variable L : inout line) is
    variable Char : character ; 
  begin
    loop 
      exit when L = NULL or L.all'length = 0 ;
      exit when L(L'left) /= ' ' and  L(L'left) /= HT ; 
      read(L, Char) ; 
    end loop ;   
  end procedure SkipSpaceTab ; 
  
  impure function EndLine(
      variable L     : inout Line 
  ) return boolean is
  begin
    return L = null or L.all'length = 0 ;
  end function EndLine ;

  impure function EmptyLine (
      variable L     : inout Line 
  ) return boolean is
  begin
    SkipSpaceTab(L) ; 
    return EndLine(L) ;
  end function EmptyLine ;

  impure function to_string (variable L : inout line; Erase : boolean := TRUE) return string is 
    variable result : string(1 to L'length) ; 
  begin
    result := L.all ; 
    if Erase then 
      deallocate (L) ; 
    end if ; 
    return result ;
  end function to_string ; 


end package body TextIOExtPkg ;