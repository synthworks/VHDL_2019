--
--  File Name:         Tb_ToString1.vhd
--  Design Unit Name:  Tb_ToString1
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis      jim@synthworks.com
--
--
--  Description:
--      VHDL-2019 To_String for vectors
--
--
--  Developed by:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    04/2021   2021.04    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  
library ieee ;
  use ieee.std_logic_1164.all ;
  use ieee.numeric_std.all ;
  use ieee.numeric_std_unsigned.all ;
  use ieee.math_real.all ;
  
  use std.env.all ;

library osvvm ;
    context osvvm.OsvvmContext ;
        
entity Tb_ToString1 is
end entity Tb_ToString1 ; 
architecture Test of Tb_ToString1 is
  constant SV :  STRING := "A String" ;
  constant BV :  BOOLEAN_VECTOR := (TRUE, TRUE, FALSE, FALSE);
  constant IV :  INTEGER_VECTOR := (1, 2, 3, 4) ;
  constant RV :  REAL_VECTOR    := (1.1, 2.2, 3.3);
  constant TV :  TIME_VECTOR    := (1 ns, 3 ns) ;

     
begin

  testProc : process
  begin
  
    log("Test 1:  to_string for vectors") ;

    print("String           = " & to_string(SV)) ; 
    print("BOOLEAN VECTOR   = " & to_string(BV)) ;
    print("INTEGER VECTOR   = " & to_string(IV)) ;
    print("REAL VECTOR      = " & to_string(RV)) ;
    print("TIME VECTOR      = " & to_string(TV)) ;

    wait ; 
  end process testProc ; 

end architecture Test ;