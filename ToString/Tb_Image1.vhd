--
--  File Name:         Tb_Image1.vhd
--  Design Unit Name:  Tb_Image1
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis      jim@synthworks.com
--
--
--  Description:
--      VHDL-2019 'image for vectors
--
--
--  Developed by:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    04/2021   2021.04    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  
library ieee ;
  use ieee.std_logic_1164.all ;
  use ieee.numeric_std.all ;
  use ieee.numeric_std_unsigned.all ;
  use ieee.math_real.all ;
  
  use std.env.all ;

library osvvm ;
    context osvvm.OsvvmContext ;
        
entity Tb_Image1 is
end entity Tb_Image1 ; 
architecture Test of Tb_Image1 is
  constant SV :  STRING := "A String" ;
  constant BV :  BOOLEAN_VECTOR := (TRUE, TRUE, FALSE, FALSE);
  constant IV :  INTEGER_VECTOR := (1, 2, 3, 4) ;
  constant RV :  REAL_VECTOR    := (1.1, 2.2, 3.3);
  constant TV :  TIME_VECTOR    := (1 ns, 3 ns) ;
  constant BT :  bit_vector     := "1010" ; 
  constant SL :  std_logic_vector := "0101" ;     
begin

  testProc : process
  begin
  
    log("Test 1:  'image for vectors") ;

    print("String           = " & string'image(SV)) ; 
    print("BOOLEAN VECTOR   = " & BOOLEAN_VECTOR'image(BV)) ;
    print("INTEGER VECTOR   = " & INTEGER_VECTOR'image(IV)) ;
    print("REAL VECTOR      = " & REAL_VECTOR'image(RV)) ;
    print("TIME VECTOR      = " & TIME_VECTOR'image(TV)) ;
    print("bit_vector       = " & bit_vector'image(BT)) ;
    print("std_logic_vector = " & std_logic_vector'image(SL)) ;
    print("String           = " & SV'image) ; 
    print("BOOLEAN VECTOR   = " & BV'image) ;
    print("INTEGER VECTOR   = " & IV'image) ;
    print("REAL VECTOR      = " & RV'image) ;
    print("TIME VECTOR      = " & TV'image) ;
    print("bit_vector       = " & BT'image) ;
    print("std_logic_vector = " & SL'image) ;

    wait ; 
  end process testProc ; 

end architecture Test ;