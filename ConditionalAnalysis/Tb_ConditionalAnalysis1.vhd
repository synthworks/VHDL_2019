--
--  File Name:         Tb_ConditionalAnalysis1.vhd
--  Design Unit Name:  Tb_ConditionalAnalysis1
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis      jim@synthworks.com
--
--
--  Description:
--      VHDL-2019 GetEnv
--
--
--  Developed by:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    04/2021   2021.04    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  
library ieee ;
  use ieee.std_logic_1164.all ;
  use ieee.numeric_std.all ;
  use ieee.numeric_std_unsigned.all ;
  use ieee.math_real.all ;
  
  use std.env.all ;

library osvvm ;
    context osvvm.OsvvmContext ;
        
entity Tb_ConditionalAnalysis1 is
end entity Tb_ConditionalAnalysis1 ; 
architecture Test of Tb_ConditionalAnalysis1 is

`if (TOOL_VENDOR = "Aldec") and (TOOL_NAME = "Riviera-PRO") then 
  constant VHDL_2019_STATUS : string := "With Aldec VHDL-2019 is here" ;

`else
  constant VHDL_2019_STATUS : string := "Still waiting to hear their VHDL-2019 plans " ;

`end if

     
begin

  testProc : process
  begin
  
    log("Test 1:  VHDL 2019 Conditional Analysis") ;
    print(VHDL_2019_STATUS) ; 

    log("Test 2:  VHDL 2019 Conditional Analysis Identifiers in VHDL") ;
    print("VHDL VERSION   = " & VHDL_VERSION) ; 
    print("TOOL VENDOR    = " & TOOL_VENDOR) ; 
    print("TOOL NAME      = " & TOOL_NAME) ; 
    print("TOOL EDITION   = " & TOOL_EDITION) ; 
    print("TOOL_VERSION   = " & TOOL_VERSION) ; 
    print("TOOL TYPE      = " & TOOL_TYPE) ; 
    print("Test Done") ; 

    wait ; 
  end process testProc ; 

end architecture Test ;