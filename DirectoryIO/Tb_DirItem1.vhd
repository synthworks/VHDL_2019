--
--  File Name:         Tb_DirItem1.vhd
--  Design Unit Name:  Tb_DirItem1
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis      jim@synthworks.com
--
--
--  Description:
--      VHDL-2019 DIR_ITEMEXISTS, DIR_ITEMISDIR, DIR_ITEMISFILE
--
--
--  Developed by:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    04/2021   2021.04    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  
library ieee ;
  use ieee.std_logic_1164.all ;
  use ieee.numeric_std.all ;
  use ieee.numeric_std_unsigned.all ;
  use ieee.math_real.all ;
  
  use std.env.all ;

library osvvm ;
    context osvvm.OsvvmContext ;
        
entity Tb_DirItem1 is
end entity Tb_DirItem1 ; 
architecture Test of Tb_DirItem1 is
     
begin

  testProc : process
  begin
    log("Test1 DIR_ITEMEXISTS") ;
    print("Directory : ../DirectoryIO    Exists = " & to_string(DIR_ITEMEXISTS("../DirectoryIO")) ) ; 
    print("FILE : ../readme.md           Exists = " & to_string(DIR_ITEMEXISTS("../readme.md")) ) ; 
    -- False: Does not exist
    print("Directory : ../FredFlintstone Exists = " & to_string(DIR_ITEMEXISTS("../FredFlintstone")) ) ; 

    log("Test1 DIR_ITEMISDIR") ;
    print("Directory : ../DirectoryIO    Is Directory = " & to_string(DIR_ITEMISDIR("../DirectoryIO")) ) ; 
    -- False: Is a file
    print("Directory : ../readme.md      Is Directory = " & to_string(DIR_ITEMISDIR("../readme.md")) ) ; 
    -- False: Does not exist
    print("Directory : ../FredFlintstone Is Directory = " & to_string(DIR_ITEMISDIR("../FredFlintstone")) ) ; 

    log("Test1 DIR_ITEMISFILE") ;
    print("FILE : ../readme.md      Is File = " & to_string(DIR_ITEMISFILE("../readme.md")) ) ; 
    -- False: Is a directory
    print("FILE : ../DirectoryIO    Is File = " & to_string(DIR_ITEMISFILE("../DirectoryIO")) ) ; 
    -- False: Does not exist
    print("FILE : ../FredFlintstone Is File = " & to_string(DIR_ITEMISFILE("../FredFlintstone")) ) ; 

    wait ; 
  end process testProc ; 

end architecture Test ;