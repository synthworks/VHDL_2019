--
--  File Name:         Tb_DirOpen1.vhd
--  Design Unit Name:  Tb_DirOpen1
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis      jim@synthworks.com
--
--
--  Description:
--      Try out:
--        -- std.textio
--        type LINE_VECTOR is array (NATURAL range <>) of LINE;
--        
--        -- std.env
--        type DIRECTORY_ITEMS is access LINE_VECTOR;
--        
--        type DIRECTORY is record
--          Name  : LINE;  -- canonical form of current directory
--          Items : DIRECTORY_ITEMS; -- pointers to items
--        end record;
--
--        type DIR_OPEN_STATUS is (
--          STATUS_OK,
--          STATUS_NOT_FOUND,
--          STATUS_NO_DIRECTORY,
--          STATUS_ACCESS_DENIED,
--          STATUS_ERROR
--        );
--        procedure DIR_OPEN (
--          Dir    : out DIRECTORY;
--          Path   : STRING;
--          Status : out DIR_OPEN_STATUS );
--          

--      VHDL-2019 DIR_ITEMEXISTS, DIR_ITEMISDIR, DIR_ITEMISFILE
--
--
--  Developed by:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    04/2021   2021.04    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  
library ieee ;
  use ieee.std_logic_1164.all ;
  use ieee.numeric_std.all ;
  use ieee.numeric_std_unsigned.all ;
  use ieee.math_real.all ;
  
  use std.env.all ;

library osvvm ;
    context osvvm.OsvvmContext ;
        
entity Tb_DirOpen1 is
end entity Tb_DirOpen1 ; 
architecture Test of Tb_DirOpen1 is
     
begin

  testProc : process
    variable Status         : dir_open_status ;
    variable Dir, SubDir    : directory ; 
  begin
    log("Test1 DIR_OPEN") ;
    
    dir_open(Dir, "..", Status) ; 
    print("Dir_open status: " & to_string(Status)) ; 
    
    print("Directory NAME: " & Dir.Name.all ) ; 
    for i in Dir.items'range loop
      next when Dir.Items(i).all = "." or Dir.Items(i).all = ".." ;
      if DIR_ITEMISDIR("../" & Dir.Items(i).all ) then
        print("  " & Dir.Items(i).all & "/") ; 
        dir_open(SubDir, "../" & Dir.Items(i).all, Status) ; 
        for i in SubDir.items'range loop
          next when SubDir.Items(i).all = "." or SubDir.Items(i).all = ".." ;
          print("    " & SubDir.Items(i).all ) ; 
        end loop ;
        dir_close(SubDir) ;
      else
        print("  " & Dir.Items(i).all) ; 
      end if ; 
    end loop ;
    dir_close(Dir) ; 
    wait ; 
  end process testProc ; 

end architecture Test ;