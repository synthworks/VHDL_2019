--
--  File Name:         TbConversionPkg_InjectErrors.vhd
--  Design Unit Name:  TbConversionPkg_InjectErrors
--  Revision:          STANDARD VERSION
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis          SynthWorks
--
--
--  Defines  TbConversionPkg_InjectErrors testbench for Mux8
--
--  Developed for:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        11898 SW 128th Ave.  Tigard, Or  97223
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    05/2021:  2021.05    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  

library ieee ; 
  use ieee.std_logic_1164.all ; 
  use ieee.numeric_std_unsigned.all ; 
  use ieee.fixed_pkg.all ; 
  
  use work.ConversionPkg.all ;
  
library osvvm ; 
  context osvvm.OsvvmContext ; 
  
entity TbConversionPkg_InjectErrors is 
end entity TbConversionPkg_InjectErrors ; 
Architecture Test of TbConversionPkg_InjectErrors is
  
  signal A          : std_logic_vector := to_slv(2, 8) ;  -- Inferring Signal Constraints from Init
  signal B          : A'subtype := to_slv(10) ;    -- Function Knows Output Subtype

--  Illegal, Error detected at compile time
--  signal C          : std_logic_vector := to_slv(5) ; 
begin

  TestProc : process
  begin
    -- Legal = OK
    -- Size of to_slv(5) = (7 downto 0)
    -- Size of result    = (7 downto 0)
    log("B : " & to_hstring(B)) ; 
    B <= A + to_slv(5) ;
    wait for 1 ns ; 
    log("B : " & to_hstring(B)) ; 
    
--  Illegal,     
--  Caught at compile time
--    (A, B) <= to_slv(255) ; 
--    wait for 1 ns ; 
--    log("B : " & to_hstring(B)) ; 

    std.env.stop(0) ; 
    wait ; 
  end process TestProc ; 
  
end architecture Test ; 