SetVHDLVersion 2019

library  default 

analyze  ConversionPkg.vhd

analyze  TbConversionPkg.vhd 
simulate TbConversionPkg

analyze  TbConversionPkg_InjectErrors.vhd 
simulate TbConversionPkg_InjectErrors

analyze  TbConversionPkg_ufixed.vhd 
simulate TbConversionPkg_ufixed