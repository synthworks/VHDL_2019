--
--  File Name:         TbConversionPkg_ufixed.vhd
--  Design Unit Name:  TbConversionPkg_ufixed
--  Revision:          STANDARD VERSION
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis          SynthWorks
--
--
--  Defines  TbConversionPkg_ufixed testbench for Mux8
--
--  Developed for:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        11898 SW 128th Ave.  Tigard, Or  97223
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    05/2021:  2021.05    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  

library ieee ; 
  use ieee.std_logic_1164.all ; 
  use ieee.numeric_std_unsigned.all ; 
  use ieee.fixed_pkg.all ; 
  
  use work.ConversionPkg.all ;
  
library osvvm ; 
  context osvvm.OsvvmContext ; 
  
entity TbConversionPkg_ufixed is 
end entity TbConversionPkg_ufixed ; 
Architecture Test of TbConversionPkg_ufixed is
  
  signal X1, X2 : ufixed(3 downto -3) := to_ufixed(2.5); 
  signal Y      : ufixed(4 downto -3) ; 
  
begin

  TestProc : process
  begin
    Y <= X1 + X2 ; 
    log("Y : " & to_string(Y)) ; 
    wait for 1 ns ; 
    
--  Following is illegal and causes run time error
--    size of to_ufixed matches target = (4 downto -3)
--    size of result = (5 downto -3) = (4 downto -3) + (3 downto -3)
    Y <= X1 + to_ufixed(1.5) ; 
    log("Y : " & to_string(Y)) ; 
    wait for 1 ns ; 

    std.env.stop(0) ; 
    wait ; 
  end process TestProc ; 
  
end architecture Test ; 