--
--  File Name:         ConversionPkg.vhd
--  Design Unit Name:  ConversionPkg
--  Revision:          STANDARD VERSION
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis          SynthWorks
--
--
--  Package Defines
--      Simple Mux4 function and procedures
--
--  Developed for:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        11898 SW 128th Ave.  Tigard, Or  97223
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    05/2021:  2021.05    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  

library ieee ; 
  use ieee.std_logic_1164.all ;
  use ieee.numeric_std_unsigned.all ;  
  use ieee.fixed_pkg.all ; 

package ConversionPkg is

  function TO_SLV (ARG : NATURAL) 
    return result of std_ulogic_vector ;
    
  function TO_UFIXED (ARG : REAL) 
    return result of ufixed ;


end package ConversionPkg ;

--- ///////////////////////////////////////////////////////////////////////////
--- ///////////////////////////////////////////////////////////////////////////
--- ///////////////////////////////////////////////////////////////////////////

package body ConversionPkg is
  
  function TO_SLV (ARG : NATURAL) 
    return result of std_ulogic_vector is
  begin
    return TO_SLV(ARG, result'length) ; 
  end function TO_SLV;
  
  function TO_UFIXED (ARG : REAL) 
    return result of ufixed is
  begin
    return TO_UFIXED(ARG, result'left, result'right) ; 
  end function TO_UFIXED;

end package body ConversionPkg ;