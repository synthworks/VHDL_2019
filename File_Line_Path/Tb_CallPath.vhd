--
--  File Name:         Tb_CallPath.vhd
--  Design Unit Name:  Tb_CallPath
--  Revision:          STANDARD VERSION
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis       SynthWorks
--
--
--  Tests:  File Composites
--
--  Developed for:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        11898 SW 128th Ave.  Tigard, Or  97223
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    06/2021:  2021.06    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  

library ieee ; 
  use ieee.std_logic_1164.all ; 
  use ieee.numeric_std.all ; 
  use ieee.numeric_std_unsigned.all ; 
  use ieee.math_real.all ; 
  
  use std.textio.all ; 
  use std.env.all ; 
  
library osvvm ; 
  context osvvm.OsvvmContext ; 
  
use work.FileLinePathPkg.all ; 
  
entity Tb_CallPath is 
end entity Tb_CallPath ; 
Architecture Test of Tb_CallPath is
 

begin


  TestProc : process
    variable int : integer := 0 ; 
    variable CallPath : CALL_PATH_VECTOR_PTR ; 
  begin
    SetLogEnable(PASSED, TRUE) ; 
    SetAlertLogName("Tb_CallPath") ;
       
    CallPath := Get_Call_Path ; 
    print("TB CallPath: " & to_string(CallPath.all)) ; 

    ProCallPath(int) ; 
    
    Print(LF & "Calling FunCallPath w/o ProCallPath") ;
    int := FunCallPath(int) ; 

    ReportAlerts ; 
    std.env.stop(0) ; 
    wait ; 
  end process TestProc ; 
  
end architecture Test ; 