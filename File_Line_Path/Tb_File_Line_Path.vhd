--
--  File Name:         Tb_File_Line_Path.vhd
--  Design Unit Name:  Tb_File_Line_Path
--  Revision:          STANDARD VERSION
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis       SynthWorks
--
--
--  Tests:  File Composites
--
--  Developed for:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        11898 SW 128th Ave.  Tigard, Or  97223
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    06/2021:  2021.06    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  

library ieee ; 
  use ieee.std_logic_1164.all ; 
  use ieee.numeric_std.all ; 
  use ieee.numeric_std_unsigned.all ; 
  use ieee.math_real.all ; 
  
  use std.textio.all ; 
  use std.env.all ; 
  
library osvvm ; 
  context osvvm.OsvvmContext ; 
  
use work.FileLinePathPkg.all ; 
  
entity Tb_File_Line_Path is 
end entity Tb_File_Line_Path ; 
Architecture Test of Tb_File_Line_Path is
 

begin


  TestProc : process
    variable int : integer := 0 ; 
  begin
    SetLogEnable(PASSED, TRUE) ; 
    SetAlertLogName("Tb_File_Line_Path") ;
       
    print("File: " & FILE_NAME & " Line: " & FILE_LINE & " Directory: " & FILE_PATH) ; 

    ProFLP(int) ; 
    int := FunFLP(int) ; 

    ReportAlerts ; 
    std.env.stop(0) ; 
    wait ; 
  end process TestProc ; 
  
end architecture Test ; 