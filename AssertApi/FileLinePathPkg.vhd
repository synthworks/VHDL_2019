--
--  File Name:         FileLinePathPkg.vhd
--  Design Unit Name:  FileLinePathPkg
--  Revision:          STANDARD VERSION
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis          SynthWorks
--
--
--  Package Defines
--      Extensions to TextIO package
--        EndLine, EmptyLine, 
--        To_String [line, boolean return string]
--
--  Developed for:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        11898 SW 128th Ave.  Tigard, Or  97223
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    06/2021:  2021.06    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  

library ieee ; 
  use ieee.std_logic_1164.all ;
  use ieee.numeric_std_unsigned.all ;  
  use ieee.fixed_pkg.all ; 
  
  use std.textio.all ; 
  use std.env.all ; 
  
library osvvm ;
  context osvvm.OsvvmContext ; 

package FileLinePathPkg is
  procedure ProFLP ( variable i : inout integer) ; 
  impure function  FunFLP (i : integer ) return integer ;

  procedure ProCallPath ( variable i : inout integer) ; 
  impure function  FunCallPath (i : integer ) return integer  ;


end package FileLinePathPkg ;

--- ///////////////////////////////////////////////////////////////////////////
--- ///////////////////////////////////////////////////////////////////////////
--- ///////////////////////////////////////////////////////////////////////////

package body FileLinePathPkg is

  procedure ProFLP ( variable i : inout integer) is
  begin
    print("File: " & FILE_NAME & " Line: " & FILE_LINE & " Directory: " & FILE_PATH) ; 
    i := i + 1 ; 
  end procedure ProFLP ; 
  
  impure function  FunFLP (i : integer ) return integer is
  begin
    print("File: " & FILE_NAME & " Line: " & FILE_LINE & " Directory: " & FILE_PATH) ; 
    return i + 1 ;
  end function FunFLP ;

  procedure ProCallPath ( variable i : inout integer) is
    variable CallPath : CALL_PATH_VECTOR_PTR ; 
  begin
    CallPath := Get_Call_Path ; 
    print("ProCallPath: " & to_string(CallPath.all)) ; 
    i := i + 1 ; 
    i := FunCallPath(i) ; 
  end procedure ProCallPath ; 
  
  impure function  FunCallPath (i : integer ) return integer is
    variable CallPath : CALL_PATH_VECTOR_PTR ; 
  begin
    CallPath := Get_Call_Path ; 
    print("FunCallPath: " & to_string(CallPath.all)) ; 
    return i + 1 ;
  end function FunCallPath ;


end package body FileLinePathPkg ;