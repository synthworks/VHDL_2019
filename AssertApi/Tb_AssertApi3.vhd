--
--  File Name:         Tb_AssertApi3.vhd
--  Design Unit Name:  Tb_AssertApi3
--  Revision:          STANDARD VERSION
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis       SynthWorks
--
--
--  Tests:  Test Assert API:  SetVhdlAssertEnable and GetVhdlAssertEnable
--
--  Developed for:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        11898 SW 128th Ave.  Tigard, Or  97223
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    06/2021:  2021.06    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  

library ieee ; 
  use ieee.std_logic_1164.all ; 
  use ieee.numeric_std.all ; 
  use ieee.numeric_std_unsigned.all ; 
  use ieee.math_real.all ; 
  
  use std.textio.all ; 
  use std.env.all ; 
  
library osvvm ; 
  context osvvm.OsvvmContext ; 
    
entity Tb_AssertApi3 is 
end entity Tb_AssertApi3 ; 
Architecture Test of Tb_AssertApi3 is
 
  file ReadFile : text open READ_MODE is FILE_PATH &  "/AssertApi3.txt" ; 

begin

  testProc : process
    variable wbuf, rbuf : line ; 
    variable A : bit_vector(3 downto 0) ;
  begin
    SetLogEnable(PASSED, TRUE) ; 
    SetAlertLogName("Tb_AssertApi3") ;
        
    -- read first line
    for i in 1 to 3 loop 
      readline(ReadFile, rbuf) ;
      read(rbuf, A) ; 
      
      case i is 
        when 1 => 
          AffirmIf(GetVhdlReadSeverity = ERROR, 
            "GetVhdlReadSeverity = " & to_string(GetVhdlReadSeverity), 
            " Expected: FAILURE") ;
          SetVhdlReadSeverity(FAILURE) ; 
        when 2 => 
          AffirmIf(GetVhdlReadSeverity = FAILURE, 
            "GetVhdlReadSeverity = " & to_string(GetVhdlReadSeverity), 
            " Expected: FAILURE") ;
          SetVhdlReadSeverity(ERROR) ; 
        when others => 
          AffirmIf(GetVhdlReadSeverity = ERROR, 
            "GetVhdlReadSeverity = " & to_string(GetVhdlReadSeverity), 
            " Expected: FAILURE") ;
      end case ; 
      wait for 10 ns ; 
    end loop ;  
    

    ReportAlerts ;
    std.env.stop(0) ; 
    wait ; 
  end process TestProc ; 
  
end architecture Test ; 