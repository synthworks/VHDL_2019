--
--  File Name:         Tb_AssertApi1.vhd
--  Design Unit Name:  Tb_AssertApi1
--  Revision:          STANDARD VERSION
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis       SynthWorks
--
--
--  Tests:  Test Assert API
--
--  Developed for:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        11898 SW 128th Ave.  Tigard, Or  97223
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    06/2021:  2021.06    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  

library ieee ; 
  use ieee.std_logic_1164.all ; 
  use ieee.numeric_std.all ; 
  use ieee.numeric_std_unsigned.all ; 
  use ieee.math_real.all ; 
  
  use std.textio.all ; 
  use std.env.all ; 
  
library osvvm ; 
  context osvvm.OsvvmContext ; 
    
entity Tb_AssertApi1 is 
end entity Tb_AssertApi1 ; 
Architecture Test of Tb_AssertApi1 is
 
  procedure SetVhdlAssertFormat( format: STRING ) is
  begin
    for I in SEVERITY_LEVEL'range loop 
      SetVhdlAssertFormat( I, format ) ; 
    end loop ; 
  end procedure SetVhdlAssertFormat ; 
  
begin


  TestProc : process
  begin
    SetLogEnable(PASSED, TRUE) ; 
    SetAlertLogName("Tb_AssertApi1") ;
    
    SetVhdlAssertFormat("%% {S}: {r} at {t} {i}") ;
    
    Print("Checks before Testing") ; 
    AffirmIfEqual(IsVhdlAssertFailed,          FALSE, "IsVhdlAssertFailed") ; 
    AffirmIfEqual(IsVhdlAssertFailed(FAILURE), FALSE, "IsVhdlAssertFailed(FAILURE)") ; 
    AffirmIfEqual(IsVhdlAssertFailed(ERROR  ), FALSE, "IsVhdlAssertFailed(ERROR)") ; 
    AffirmIfEqual(IsVhdlAssertFailed(WARNING), FALSE, "IsVhdlAssertFailed(WARNING)") ; 

    AffirmIfEqual(GetVhdlAssertCount,              0, "GetVhdlAssertCount") ; 
    AffirmIfEqual(GetVhdlAssertCount(FAILURE),     0, "GetVhdlAssertCount(FAILURE)") ; 
    AffirmIfEqual(GetVhdlAssertCount(ERROR  ),     0, "GetVhdlAssertCount(ERROR)") ; 
    AffirmIfEqual(GetVhdlAssertCount(WARNING),     0, "GetVhdlAssertCount(WARNING)") ; 
    
    Print("Error Count before Testing") ; 
    Print("Total Errors: " & to_string(GetVhdlAssertCount) &
        ", Failures: " & to_string(GetVhdlAssertCount(FAILURE)) &
        ", Errors: "   & to_string(GetVhdlAssertCount(ERROR)) &
        ", Warnings: " & to_string(GetVhdlAssertCount(WARNING)));
       
    Print("Insert One Error and One Note") ; 
    assert FALSE report "E1" severity ERROR ; 
    assert FALSE report "N1" severity NOTE ; 
    
    AffirmIfEqual(IsVhdlAssertFailed,          TRUE,  "IsVhdlAssertFailed") ; 
    AffirmIfEqual(IsVhdlAssertFailed(FAILURE), FALSE, "IsVhdlAssertFailed(FAILURE)") ; 
    AffirmIfEqual(IsVhdlAssertFailed(ERROR  ), TRUE,  "IsVhdlAssertFailed(ERROR)") ; 
    AffirmIfEqual(IsVhdlAssertFailed(WARNING), FALSE, "IsVhdlAssertFailed(WARNING)") ; 

    AffirmIfEqual(GetVhdlAssertCount,              1, "GetVhdlAssertCount") ; 
    AffirmIfEqual(GetVhdlAssertCount(FAILURE),     0, "GetVhdlAssertCount(FAILURE)") ; 
    AffirmIfEqual(GetVhdlAssertCount(ERROR  ),     1, "GetVhdlAssertCount(ERROR)") ; 
    AffirmIfEqual(GetVhdlAssertCount(WARNING),     0, "GetVhdlAssertCount(WARNING)") ; 

    Print("Total Errors: " & to_string(GetVhdlAssertCount) &
        ", Failures: " & to_string(GetVhdlAssertCount(FAILURE)) &
        ", Errors: "   & to_string(GetVhdlAssertCount(ERROR)) &
        ", Warnings: " & to_string(GetVhdlAssertCount(WARNING)));


    Print("Change Assert Format to:  %% {S} at {t:+>10.ns}  {r}") ; 

    SetVhdlAssertFormat("%% {S} at {t:+>10.ns}  {r}") ;

    Print("Add one Error and one Warning") ; 
    assert FALSE report "E2" ; 
    wait for 100 ns ; 
    assert FALSE report "W1" severity WARNING ; 
    wait for 9900 ns ; 

    AffirmIfEqual(IsVhdlAssertFailed,          TRUE,  "IsVhdlAssertFailed") ; 
    AffirmIfEqual(IsVhdlAssertFailed(FAILURE), FALSE, "IsVhdlAssertFailed(FAILURE)") ; 
    AffirmIfEqual(IsVhdlAssertFailed(ERROR  ), TRUE,  "IsVhdlAssertFailed(ERROR)") ; 
    AffirmIfEqual(IsVhdlAssertFailed(WARNING), TRUE,  "IsVhdlAssertFailed(WARNING)") ; 

    AffirmIfEqual(GetVhdlAssertCount,              3, "GetVhdlAssertCount") ; 
    AffirmIfEqual(GetVhdlAssertCount(FAILURE),     0, "GetVhdlAssertCount(FAILURE)") ; 
    AffirmIfEqual(GetVhdlAssertCount(ERROR  ),     2, "GetVhdlAssertCount(ERROR)") ; 
    AffirmIfEqual(GetVhdlAssertCount(WARNING),     1, "GetVhdlAssertCount(WARNING)") ; 

    Print("Total Errors: " & to_string(GetVhdlAssertCount) &
        ", Failures: " & to_string(GetVhdlAssertCount(FAILURE)) &
        ", Errors: "   & to_string(GetVhdlAssertCount(ERROR)) &
        ", Warnings: " & to_string(GetVhdlAssertCount(WARNING)));

    
    Print("Clear Asserts") ; 
    ClearVhdlAssert ;   

    AffirmIfEqual(IsVhdlAssertFailed,          FALSE, "IsVhdlAssertFailed") ; 
    AffirmIfEqual(IsVhdlAssertFailed(FAILURE), FALSE, "IsVhdlAssertFailed(FAILURE)") ; 
    AffirmIfEqual(IsVhdlAssertFailed(ERROR  ), FALSE, "IsVhdlAssertFailed(ERROR)") ; 
    AffirmIfEqual(IsVhdlAssertFailed(WARNING), FALSE, "IsVhdlAssertFailed(WARNING)") ; 

    AffirmIfEqual(GetVhdlAssertCount,              0, "GetVhdlAssertCount") ; 
    AffirmIfEqual(GetVhdlAssertCount(FAILURE),     0, "GetVhdlAssertCount(FAILURE)") ; 
    AffirmIfEqual(GetVhdlAssertCount(ERROR  ),     0, "GetVhdlAssertCount(ERROR)") ; 
    AffirmIfEqual(GetVhdlAssertCount(WARNING),     0, "GetVhdlAssertCount(WARNING)") ; 
    
    Print("Error Count After Clear Assert") ; 
    Print("Total Errors: " & to_string(GetVhdlAssertCount) &
        ", Failures: " & to_string(GetVhdlAssertCount(FAILURE)) &
        ", Errors: "   & to_string(GetVhdlAssertCount(ERROR)) &
        ", Warnings: " & to_string(GetVhdlAssertCount(WARNING)));

    Print("Add one FAILURE") ; 
    assert FALSE report "F1" severity FAILURE ; 

    AffirmIfEqual(IsVhdlAssertFailed,          TRUE,  "IsVhdlAssertFailed") ; 
    AffirmIfEqual(IsVhdlAssertFailed(FAILURE), TRUE,  "IsVhdlAssertFailed(FAILURE)") ; 
    AffirmIfEqual(GetVhdlAssertCount,              1, "GetVhdlAssertCount") ; 
    AffirmIfEqual(GetVhdlAssertCount(FAILURE),     1, "GetVhdlAssertCount(FAILURE)") ; 

    Print("Total Errors: " & to_string(GetVhdlAssertCount) &
        ", Failures: " & to_string(GetVhdlAssertCount(FAILURE)) &
        ", Errors: "   & to_string(GetVhdlAssertCount(ERROR)) &
        ", Warnings: " & to_string(GetVhdlAssertCount(WARNING)));

    ReportAlerts ;
    std.env.stop(0) ; 
    wait ; 
  end process TestProc ; 
  
end architecture Test ; 