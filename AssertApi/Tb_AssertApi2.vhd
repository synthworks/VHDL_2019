--
--  File Name:         Tb_AssertApi2.vhd
--  Design Unit Name:  Tb_AssertApi2
--  Revision:          STANDARD VERSION
--
--  Maintainer:        Jim Lewis      email:  jim@synthworks.com
--  Contributor(s):
--     Jim Lewis       SynthWorks
--
--
--  Tests:  Test Assert API:  SetVhdlAssertEnable and GetVhdlAssertEnable
--
--  Developed for:
--        SynthWorks Design Inc.
--        VHDL Training Classes
--        11898 SW 128th Ave.  Tigard, Or  97223
--        http://www.SynthWorks.com
--
--  Revision History:
--    Date      Version    Description
--    06/2021:  2021.06    Initial revision
--
--  
--  Copyright (c) 2021 by SynthWorks Design Inc.  
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      https://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  

library ieee ; 
  use ieee.std_logic_1164.all ; 
  use ieee.numeric_std.all ; 
  use ieee.numeric_std_unsigned.all ; 
  use ieee.math_real.all ; 
  
  use std.textio.all ; 
  use std.env.all ; 
  
library osvvm ; 
  context osvvm.OsvvmContext ; 
    
entity Tb_AssertApi2 is 
end entity Tb_AssertApi2 ; 
Architecture Test of Tb_AssertApi2 is
 
  procedure SetVhdlAssertFormat( format: STRING ) is
  begin
    for I in SEVERITY_LEVEL'range loop 
      SetVhdlAssertFormat( I, format ) ; 
    end loop ; 
  end procedure SetVhdlAssertFormat ; 
  
begin


  TestProc : process
  begin
    SetLogEnable(PASSED, TRUE) ; 
    SetAlertLogName("Tb_AssertApi2") ;
    
    SetVhdlAssertFormat("%% {S}: {r} at {t:.ns}") ;
    wait for 10 ns ; 
    
    Print("Checks with SetVhdlAssertEnable(FALSE)") ; 
    SetVhdlAssertEnable(FALSE) ;
    assert FALSE report "F1" severity FAILURE ; 
    assert FALSE report "E1" severity ERROR ; 
    assert FALSE report "W1" severity WARNING ; 
    
    AffirmIfEqual(IsVhdlAssertFailed,           FALSE, "IsVhdlAssertFailed") ; 
    AffirmIfEqual(IsVhdlAssertFailed(FAILURE),  FALSE, "IsVhdlAssertFailed(FAILURE)") ; 
    AffirmIfEqual(IsVhdlAssertFailed(ERROR  ),  FALSE, "IsVhdlAssertFailed(ERROR)") ; 
    AffirmIfEqual(IsVhdlAssertFailed(WARNING),  FALSE, "IsVhdlAssertFailed(WARNING)") ; 

    AffirmIfEqual(GetVhdlAssertCount,               0, "GetVhdlAssertCount") ; 
    AffirmIfEqual(GetVhdlAssertCount(FAILURE),      0, "GetVhdlAssertCount(FAILURE)") ; 
    AffirmIfEqual(GetVhdlAssertCount(ERROR  ),      0, "GetVhdlAssertCount(ERROR)") ; 
    AffirmIfEqual(GetVhdlAssertCount(WARNING),      0, "GetVhdlAssertCount(WARNING)") ; 
    
    AffirmIfEqual(GetVhdlAssertEnable(FAILURE), FALSE, "GetVhdlAssertEnable(FAILURE)") ; 
    AffirmIfEqual(GetVhdlAssertEnable(ERROR  ), FALSE, "GetVhdlAssertEnable(ERROR)") ; 
    AffirmIfEqual(GetVhdlAssertEnable(WARNING), FALSE, "GetVhdlAssertEnable(WARNING)") ; 
    wait for 10 ns ; 
    
    Print("Checks with SetVhdlAssertEnable(TRUE) and ERROR") ; 
    SetVhdlAssertEnable(ERROR, TRUE) ;
    SetVhdlAssertEnable(TRUE) ;
    assert FALSE report "E2" severity ERROR ; 
    AffirmIfEqual(GetVhdlAssertCount (ERROR  ),     1, "GetVhdlAssertCount(ERROR)") ; 
    AffirmIfEqual(GetVhdlAssertEnable(ERROR  ),  TRUE, "GetVhdlAssertEnable(ERROR)") ; 
    
    Print("Checks with SetVhdlAssertEnable(ERROR, FALSE)") ; 
    SetVhdlAssertEnable(ERROR, FALSE) ;
    assert FALSE report "E3" severity ERROR ; 
    AffirmIfEqual(GetVhdlAssertCount (ERROR  ),     1, "GetVhdlAssertCount(ERROR)") ; 
    AffirmIfEqual(GetVhdlAssertEnable(ERROR  ), FALSE, "GetVhdlAssertEnable(ERROR)") ; 

    Print("Checks with SetVhdlAssertEnable(ERROR, TRUE)") ; 
    SetVhdlAssertEnable(ERROR, TRUE) ;
    assert FALSE report "E4" severity ERROR ; 
    AffirmIfEqual(GetVhdlAssertCount (ERROR  ),     2, "GetVhdlAssertCount(ERROR)") ; 
    AffirmIfEqual(GetVhdlAssertEnable(ERROR  ),  TRUE, "GetVhdlAssertEnable(ERROR)") ; 
    
    Print("Checks with SetVhdlAssertEnable(ERROR, FALSE), Global Enable to TRUE") ; 
    SetVhdlAssertEnable(ERROR, FALSE) ;
    SetVhdlAssertEnable(TRUE) ;
    assert FALSE report "E5" severity ERROR ; 
    AffirmIfEqual(GetVhdlAssertCount (ERROR  ),     3, "GetVhdlAssertCount(ERROR)") ; 
    AffirmIfEqual(GetVhdlAssertEnable(ERROR  ),  TRUE, "GetVhdlAssertEnable(ERROR)") ; 

    Print("Checks with SetVhdlAssertEnable(TRUE) and Warning") ; 
    SetVhdlAssertEnable(WARNING, TRUE) ;
    SetVhdlAssertEnable(TRUE) ;
    assert FALSE report "W2" severity WARNING ; 
    AffirmIfEqual(GetVhdlAssertCount (WARNING),     1, "GetVhdlAssertCount(WARNING)") ; 
    AffirmIfEqual(GetVhdlAssertEnable(WARNING),  TRUE, "GetVhdlAssertEnable(WARNING)") ; 
    
    Print("Checks with SetVhdlAssertEnable(WARNING, FALSE)") ; 
    SetVhdlAssertEnable(WARNING, FALSE) ;
    assert FALSE report "W3" severity WARNING ; 
    AffirmIfEqual(GetVhdlAssertCount (WARNING),     1, "GetVhdlAssertCount(WARNING)") ; 
    AffirmIfEqual(GetVhdlAssertEnable(WARNING), FALSE, "GetVhdlAssertEnable(WARNING)") ; 

    Print("Checks with SetVhdlAssertEnable(WARNING, TRUE)") ; 
    SetVhdlAssertEnable(WARNING, TRUE) ;
    assert FALSE report "W4" severity WARNING ; 
    AffirmIfEqual(GetVhdlAssertCount (WARNING),     2, "GetVhdlAssertCount(WARNING)") ; 
    AffirmIfEqual(GetVhdlAssertEnable(WARNING),  TRUE, "GetVhdlAssertEnable(WARNING)") ; 
    
    Print("Override with SetVhdlAssertEnable(WARNING, FALSE), Global Enable to TRUE") ; 
    SetVhdlAssertEnable(WARNING, FALSE) ;
    SetVhdlAssertEnable(TRUE) ;
    assert FALSE report "W5" severity WARNING ; 
    AffirmIfEqual(GetVhdlAssertCount (WARNING),     3, "GetVhdlAssertCount(WARNING)") ; 
    AffirmIfEqual(GetVhdlAssertEnable(WARNING),  TRUE, "GetVhdlAssertEnable(WARNING)") ; 
    

    ReportAlerts ;
    std.env.stop(0) ; 
    wait ; 
  end process TestProc ; 
  
end architecture Test ; 