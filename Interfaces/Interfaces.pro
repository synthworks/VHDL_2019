SetVHDLVersion 2019

puts "Building OSVVM Libraries"
build ../OsvvmLibraries

puts "Running AxiStream testbench"
build ../OsvvmLibraries/AXI4/AxiStream/testbench

# puts "Running AXI4 testbench"
# build ../OsvvmLibraries/AXI4/Axi4/testbench
